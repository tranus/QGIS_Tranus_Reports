######## Project organization ##########

* /src : Folder with sources codes
	/docs : Latex of the documentation
	/tranus_qgis_plugin : QGIS plugin
        /standalone : TRANUS standalone report displayer
* /docs : Documentation

######### Useful commands ##############

* Fast webserver for testing the 3d render HTML

python -m SimpleHTTPServer

* Reload plugin in QGIS

qgis.utils.reloadPlugin('tranusprocessor')

######### From Patricio ##############

* Create a hardlink to a folder on Windows (maybe for pointing the QGIS plugin to this project)

mklink /J C:\Users\pinzaghi\.qgis2\python\plugins\tranusprocessor C:\Users\pinzaghi\INRIA\qgistranus\src\tranus_qgis_plugin

* Examples of usage for the standalone version

python tranus_reports.py thematic --render2d --logscale ..\..\..\misc\grenoble_maps\sectir2010.json ..\..\..\misc\sample_data\report_sectir2010.txt 0 4 3 0 10

* ONE TO MANY REPORT

python tranus_reports.py one2many --render3d_path one2many ..\..\..\misc\grenoble_maps\sectir2010.json ..\..\..\misc\sample_data\report_sectir2010.txt ..\..\..\misc\sample_data\relations_data_sectir2010.txt 0 4 0

python tranus_reports.py one2many --colors 255:0:0 0:255:0 0:0:255 --range 0:40 40:80 80:100 --render3d_path one2many ..\..\..\misc\grenoble_maps\sectir2010.json ..\..\..\misc\sample_data\report_sectir2010.txt ..\..\..\misc\sample_data\relations_data_sectir2010.txt 0 4 5
