import sys
from PyQt4 import QtCore
from PyQt4 import QtGui
from PyQt4 import uic
from qgis.core import *

# initialize Qt resources from file resources.py
import resources

# Import the code for the dialogs
from generate_report import GenerateReportDialog

class TranusReports:
    ''' Class for initialize the plugin, add it to the menu and connect signals '''
    def __init__(self, iface):
        # save reference to the QGIS interface
        self.iface = iface

    def initGui(self):         
        # create actionGenerateReport that will load TRANUS outcome file
        self.actionGenerateReport = QtGui.QAction(QtGui.QIcon(":/plugins/tranus_qgis_plugin/icon.png"), "Generate report", \
          self.iface.mainWindow())
        self.actionGenerateReport.setObjectName("generateReport")
        QtCore.QObject.connect(self.actionGenerateReport, QtCore.SIGNAL("triggered()"), self.generateReport_run)

        self.iface.addToolBarIcon(self.actionGenerateReport)
        self.iface.addPluginToMenu("&TRANUS Reports", self.actionGenerateReport)

    def unload(self):
        # remove the plugin menu item and icon
        self.iface.removePluginMenu("&TRANUS Reports",self.actionGenerateReport)
        self.iface.removeToolBarIcon(self.actionGenerateReport)


    def generateReport_run(self):
        self.generate_report_dlg = GenerateReportDialog(self.iface)
        self.generate_report_dlg.show() 
        # Run the dialog event loop
        result = self.generate_report_dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass
    
    