import sys, os

from imp import reload
# We add the script directory to the enviroment paths
sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from qgis.gui import *

from generate_report_ui import Ui_GenerateReportDialog
import reports
from reports.base_report import *
from reports.thematic_report import ThematicMapReport
from interface import *

class GenerateReportDialog(QtGui.QDialog):
    def __init__(self, qgis_iface):

        reload(reports.thematic_report)
        reload(reports.base_report)


        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.ui = Ui_GenerateReportDialog()
        self.ui.setupUi(self)
        

        # Instance an Interface object to interact between the GUI and QGIS API
        self.interface = Interface(qgis=qgis_iface, gui=self.ui)
        
        # Instance an empty report
        self.report = BaseReport()
        
        # Config GUI dialog for the report
        self.config_dlg = None

        # Connect the browse file button with its action
        QtCore.QObject.connect(self.ui.browseReportFileButton_Widget, QtCore.SIGNAL('clicked()'), self.selectReportFile_Action)
        
        # Connect the open button with the validate function
        QtCore.QObject.connect(self.ui.loadReportFileButton_Widget, QtCore.SIGNAL('clicked()'), self.loadReport_Action)
        QtCore.QObject.connect(self.ui.generateButton_Widget, QtCore.SIGNAL('clicked()'), self.generateReport_Action)
        QtCore.QObject.connect(self.ui.cancelButton_Widget, QtCore.SIGNAL('clicked()'), self.close)
                
        # Fill the combo with the report types
        self.ui.reportTypeCombo_Widget.clear()
        self.ui.reportTypeCombo_Widget.addItem("", None)
        for item in BaseReport.getReportTypes():
            self.ui.reportTypeCombo_Widget.addItem(item["label"], item["class_name"])
            
        # Fill the layers combo with the available layers on the proyect
        self.ui.baseMapLayerCombo_Widget.clear()
        self.ui.baseMapLayerCombo_Widget.addItem("", None)
        layers = self.interface.qgis.legendInterface().layers()
        for layer in layers:
            layerId = layer.id()
            layerName = layer.name()
            layerType = layer.type()
            if layerType == QgsMapLayer.VectorLayer:
                self.ui.baseMapLayerCombo_Widget.addItem(layerName, layer)
                
    def closeEvent(self, event):
        pass
        
    def selectReportFile_Action(self):
        ''' Display browse file dialog '''
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Select file')
        if fname:
            self.ui.reportFileInput_Widget.setText(fname)
                        
    def loadReport_Action(self):
        ''' Make some validations and load the report '''

        base_map_layer_index = self.ui.baseMapLayerCombo_Widget.currentIndex()
        delimiter_str = self.ui.delimiterInput_Widget.text()
        
        if self.ui.rowsRadioButton_Widget.isChecked():
            axis = 'row'
        else:
            axis = 'column'
        
        report_str = self.ui.reportFileInput_Widget.text()
        
        # Validations
        if base_map_layer_index == 0:
            msgBox = QMessageBox.warning(self, "Error", "You have to select a 'Map layer'.") 
        elif delimiter_str == '':
            msgBox = QMessageBox.warning(self, "Error", "Delimiter cannot be empty.")
        elif report_str == '':
            msgBox = QMessageBox.warning(self, "Error", "You have to select a 'Matrix data file'.")    
        else: 
            # Selected layer
            selected_layer = self.interface.qgis.legendInterface().layers()[base_map_layer_index-1]
            
            try:
                self.report = BaseReport(interface=self.interface, map_layer=selected_layer ,report_file=report_str )
                self.report.load(report_delimiter=delimiter_str, zones_axis=axis)
            except ReportError as e:
                msgBox = QMessageBox.critical(self, "Error", str(e)) 
                
            # Update the dialog with the loaded report data    
            if self.report.isLoaded():    
                self.ui.previewTableView_Widget.setEnabled(True)
                self.ui.reportTypeCombo_Widget.setEnabled(True)
                self.ui.joinComboBox_Widget.setEnabled(True)
                self.ui.join2ComboBox_Widget.setEnabled(True)
                
                ### Preview the data ###
                report_data_mtx = self.report.getReportDataMtx()

                rows_length = len(report_data_mtx)              
                    
                table_model = QtGui.QStandardItemModel(rows_length, len(report_data_mtx[0]), self.ui.previewTableView_Widget)
                
                for i, row in enumerate(report_data_mtx):
                    for j, col in enumerate(row):
                        item = QStandardItem(str(report_data_mtx[i][j]))
                        table_model.setItem(i, j, item)                 
                
                table_model.setHorizontalHeaderLabels(report_data_mtx.dtype.names) # matrix data
                self.ui.previewTableView_Widget.setModel(table_model)               
                
                # Reset combos
                self.ui.joinComboBox_Widget.clear()
                self.ui.join2ComboBox_Widget.clear()
                
                # Fill the first join combo with the map layer fields
                fields = self.report.getLayerFields()
                for field in fields:
                    self.ui.joinComboBox_Widget.addItem(field.name(), field)
                
                # If we selected that the zones are in the rows, we ask for the row index for the join
                for field in self.report.getJoinedFields():
                    self.ui.join2ComboBox_Widget.addItem( field['label'] , field['value'])
        
    def generateReport_Action(self):
    
        report_type_index = self.ui.reportTypeCombo_Widget.currentIndex()
        
        if not self.report.isLoaded():
            msgBox = QMessageBox.warning(self, "Error", "There is no report file loaded.")
        elif report_type_index == 0:
            msgBox = QMessageBox.warning(self, "Error", "You have to select a 'Report type'.")
        else:    
            # Selected report type
            selected_item = BaseReport.getReportTypes()[report_type_index-1]
            
            # Python magic for dynamic class instantiation
            pkg = __import__('reports', fromlist=[selected_item["module_name"]])
            module = getattr(pkg, selected_item["module_name"])
            report_class = getattr(module, selected_item["class_name"])
            report_gui_class = getattr(module, selected_item["class_name"]+"_Dialog")
            
            # Selected layer
            base_map_layer_index = self.ui.baseMapLayerCombo_Widget.currentIndex()
            selected_layer = self.interface.qgis.legendInterface().layers()[base_map_layer_index-1]
            
            report_str = self.ui.reportFileInput_Widget.text()
            
            # Change the report class type for the selected one 
            self.report.__class__ = report_class
            self.report.__init__(map_layer=selected_layer ,report_file=report_str, parent_widget=self )
             
            # We run the GUI dialog for getting the extra parameters for this report
            self.config_dlg = report_gui_class( self.report, self)
            self.config_dlg.show() 
            # Run the dialog event loop, this will ask for extra input parameters
            result = self.config_dlg.exec_()
            
            # This line will be executed when the report config has finished
            self.config_dlg.close()
    
  
    