from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from qgis.gui import *

class Interface:
    """ Object to communicate the user parameters, QGIS API and the reports objects
    """
    
    def __init__(self, qgis=None, gui=None):
        self.qgis = qgis
        self.gui = gui
        self.parameters = {}
        
    def getParameter(self, key):
        if key in self.parameters:
            return self.parameters[key]
        else:
            print "The parameter "+key+" is not defined"
        
    def setParameter(self, key, value):
        self.parameters[key] = value