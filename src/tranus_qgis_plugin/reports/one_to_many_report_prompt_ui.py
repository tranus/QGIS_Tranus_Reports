# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\pinzaghi\INRIA\qgistranus\src\tranus_qgis_plugin\reports\one_to_many_report_prompt.ui'
#
# Created: Wed Jan 07 14:13:16 2015
#      by: PyQt4 UI code generator 4.11.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_OneToManyReportPromptDialog(object):
    def setupUi(self, OneToManyReportPromptDialog):
        OneToManyReportPromptDialog.setObjectName(_fromUtf8("OneToManyReportPromptDialog"))
        OneToManyReportPromptDialog.setWindowModality(QtCore.Qt.WindowModal)
        OneToManyReportPromptDialog.resize(243, 71)
        self.verticalLayout = QtGui.QVBoxLayout(OneToManyReportPromptDialog)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(OneToManyReportPromptDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.nameLineEdit_Widget = QtGui.QLineEdit(OneToManyReportPromptDialog)
        self.nameLineEdit_Widget.setObjectName(_fromUtf8("nameLineEdit_Widget"))
        self.horizontalLayout.addWidget(self.nameLineEdit_Widget)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.saveButton_Widget = QtGui.QPushButton(OneToManyReportPromptDialog)
        self.saveButton_Widget.setObjectName(_fromUtf8("saveButton_Widget"))
        self.horizontalLayout_2.addWidget(self.saveButton_Widget)
        self.cancelButton_Widget = QtGui.QPushButton(OneToManyReportPromptDialog)
        self.cancelButton_Widget.setObjectName(_fromUtf8("cancelButton_Widget"))
        self.horizontalLayout_2.addWidget(self.cancelButton_Widget)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(OneToManyReportPromptDialog)
        QtCore.QMetaObject.connectSlotsByName(OneToManyReportPromptDialog)

    def retranslateUi(self, OneToManyReportPromptDialog):
        OneToManyReportPromptDialog.setWindowTitle(_translate("OneToManyReportPromptDialog", "Dialog", None))
        self.label.setText(_translate("OneToManyReportPromptDialog", "Name:", None))
        self.saveButton_Widget.setText(_translate("OneToManyReportPromptDialog", "Save", None))
        self.cancelButton_Widget.setText(_translate("OneToManyReportPromptDialog", "Cancel", None))

