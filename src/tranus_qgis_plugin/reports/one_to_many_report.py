from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from qgis.gui import *

from one_to_many_report_ui import Ui_OneToManyReportDialog

from base_report import *

import random, math, os, shutil, sys, json, imp, os.path, numpy

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'\..')

class OneToManyReport(BaseReport):
    ''' This report generate a map coloured with the relation between the values of one region with the others
    '''
    MODES_TITLES = {QgsGraduatedSymbolRendererV2.EqualInterval: "Equal Interval", QgsGraduatedSymbolRendererV2.Custom: "Custom"}
    
    def __init__(self, report_file=None, relations_file=None, map_layer=None, parent_widget=None):  
        report_data_mtx = []
        
        self.relations_file = relations_file

        # Create a dict for indexing the report by joined fields
        self.report_data_dict = {}   
        self.settings = QSettings("inria", "tranusreports")
        # self.ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))
    
    def renderReport(self, layer):
        ''' Render the report in the map layer '''

        # Selected graduated modes
        mode = self.interface.getParameter('mode')
        rangeList = self.interface.getParameter('rangeList')

        render3d = self.interface.getParameter('render3d')
        render2d = self.interface.getParameter('render2d')
        color = self.interface.getParameter('colorMode')
        color1 = self.strToQColor(self.interface.getParameter('colorscale_start'))
        color2 = self.strToQColor(self.interface.getParameter('colorscale_end'))
        
        layer_2d = None
        if render2d: 
            layer_2d = self.copyLayer2d(layer, useLogscale=False)
            if layer_2d.isValid():
                # label = QgsPalLayerSettings()
                # label.readFromLayer(layer_2d)
                # label.enabled = True
                # label.fieldName = 'FLUX'
                # label.placement= QgsPalLayerSettings.AroundPoint
                # label.setDataDefinedProperty(QgsPalLayerSettings.Size,True,True,'8','')
                # label.writeToLayer(layer_2d)

                if color:
                    self.applyCategorieSymbologyStandardMode(layer_2d)

                QgsMapLayerRegistry.instance().addMapLayers([layer_2d])

            else:
                print "Ignored flag, is only valid for Custom and EqualInterval"

        layer_3d = None    
        if render3d:
            if mode == QgsGraduatedSymbolRendererV2.EqualInterval or mode == QgsGraduatedSymbolRendererV2.Custom:
                layer_3d = self.copyLayer3d(layer, useLogscale=False)
                if layer_3d.isValid():
                    self.generate3Drender(layer_3d, mode)
            else:
                raise ReportError('The only modes supported are EqualInterval and Custom')
                
        return {'layer2d': layer_2d, 'layer3d': layer_3d}
                
    
    # copylayer for render2d    
    def copyLayer2d(self, base_map, useLogscale):
        ''' Return a copy of the layer with a new field with the report data '''        
        # One row = one zone with data, the join field is in a column of the row
        report_data_mtx = self.getReportDataMtx()
        join2_field_idx = self.interface.getParameter('join2_field_idx')
        mode = self.interface.getParameter('mode')
        delimiter_str = self.interface.getParameter('delimiterRelation')
        skipHeader = int(self.interface.getParameter('skipHeader'))
        rangeList = self.interface.getParameter('rangeList')
        factor = float(self.interface.getParameter('factorArrow'))
        colorMode = self.interface.getParameter('colorMode')


        rangeList_obj = {}
        if(mode == QgsGraduatedSymbolRendererV2.Custom):
            for elem in rangeList:
                rangeList_obj["lowerValue"] = elem.lowerValue() 
                rangeList_obj["upperValue"] = elem.upperValue() 
                rangeList_obj["red"] = elem.symbol().color().red()
                rangeList_obj["green"] = elem.symbol().color().green()
                rangeList_obj["blue"] = elem.symbol().color().blue() 
        # else:
        #     rangeList_obj.append( {"lowerValue": range.lowerValue(), "upperValue": range.upperValue(), "red":255 , "green":0 , "blue":0 } ) 
        
        # We have one row at least
        if join2_field_idx >= len(report_data_mtx[0]):
            raise ReportError('The join field index of the report is out of range.')
        
        for row in report_data_mtx:
            self.report_data_dict[row[join2_field_idx]] = row
        
        # Join column of the layer
        join_field_idx = self.interface.getParameter('join_field_idx')
        
        mode_title = self.interface.getParameter('mode_title')
        # Selected field for the report
        joinedFields = self.getJoinedFields()

        provider_map_layer = base_map.dataProvider()

        vl = base_map
        # layer = QgsVectorLayer("LineString", "line", "memory")
        layer =  QgsVectorLayer('Polygon', 'Relations' , "memory")

        layer.startEditing()
        provider = layer.dataProvider()

        attributesVar = [(QgsField("ORIGINE", QVariant.String)) , (QgsField("DEST", QVariant.String)), (QgsField("FLUX", QVariant.Double))]
        provider.addAttributes(attributesVar)
        # layer.updateFields()

        # table relation 
        self.relations_file = self.interface.getParameter('relations_path')        
        relations_data = numpy.genfromtxt(self.relations_file, skip_header=skipHeader, dtype=None, names=True, delimiter=delimiter_str)

        number_zone = len(relations_data)-1 #the last line of relations_data is the sum total 

        # list of zone id
        zone_id = []
        total = {}
        row_index = 0
        for row in relations_data:
            id = row[0].split()[0]
            if id.isdigit():
                zone_id.append(int(id))
                total[id]=row[len(row)-1]
            row_index +=1

        # relations dictionary
        row_index = 0
        maxValue = 0
        relations_data_dict = {}
        for row in relations_data:
            if (row_index <= number_zone-1): # number_zone-1) :
                r = int(zone_id[row_index])
                relations_data_dict[r] = {}
                column_index = 1
                while (column_index<=number_zone-1):
                    value = row[column_index]
                    c = int(zone_id[column_index-1])
                    if (isinstance(value,str)==False) & (value!=0) & (r!=c):
                        relations_data_dict[r][c] = value
                        if maxValue < value:
                            maxValue = value
                    column_index += 1
                row_index += 1    

        # the coordinates of the centroids of the features from a layer
        coordinates = self.createDictionnary(base_map,join_field_idx)

        flowTable, lengthArrows = self.flowList(relations_data_dict, coordinates, number_zone)   

        lengths_mean = sum(lengthArrows) / (len(lengthArrows))
        lengths_min = min(lengthArrows)
        head_length = (lengths_mean / 15.0)     
        head_overwidth = (head_length / 2.0)


        #List of arrows    
        if(mode == QgsGraduatedSymbolRendererV2.Custom):
            lowerValue = rangeList_obj['lowerValue'] 
            upperValue = rangeList_obj['upperValue']
        else: 
            lowerValue = 0
            upperValue = maxValue

        listArrow = []
        for elem in flowTable:
            if elem[1] != elem[2]:
                if (elem[0] >= lowerValue) & (elem[0] <= upperValue):
                    offset = 0
                    id = elem[1]

                    # width of arrow
                    width = elem[0]*factor 
                    
                    # width = 1
                    # if (colorMode):
                    #     width = 1    
                    # else : 
                    #     width = elem[0]*factor 
        
                        
                    po = QgsPoint(elem[3])      # point origin                
                    pd = QgsPoint(elem[4])      # point destination

                    length = math.sqrt((pd[0]-po[0])**2 + (pd[1]-po[1])**2)
                    unitVector = ((pd[0]-po[0])/length,(pd[1]-po[1])/length) 

                    coeff_pm = 500
                    if(po[0]<pd[0]):
                        if (po[1]<pd[1]):
                            po = (po[0]+unitVector[0],po[1]+unitVector[1])
                            pd = (pd[0]-unitVector[0],pd[1]-unitVector[1])
                            pm = QgsPoint((po[0]+pd[0])/2-coeff_pm, (po[1]+pd[1])/2-coeff_pm)
                        else:
                            po = (po[0]+unitVector[0],po[1]-unitVector[1])
                            pd = (pd[0]-unitVector[0],pd[1]+unitVector[1])
                            pm = QgsPoint((po[0]+pd[0])/2-coeff_pm, (po[1]+pd[1])/2-coeff_pm)
                    else:
                        if (po[1]<pd[1]):
                            po = (po[0]-unitVector[0],po[1]+unitVector[1])
                            pd = (pd[0]+unitVector[0],pd[1]-unitVector[1])
                            pm = QgsPoint((po[0]+pd[0])/2+coeff_pm, (po[1]+pd[1])/2+coeff_pm)
                        else:
                            po = (po[0]-unitVector[0],po[1]-unitVector[1])
                            pd = (pd[0]+unitVector[0],pd[1]+unitVector[1])
                            pm = QgsPoint((po[0]+pd[0])/2+coeff_pm, (po[1]+pd[1])/2+coeff_pm)
                        

                    # circle from 3 points    
                    circle = self.pointstocircle(po, pm, pd)

                    # Calculate angles
                    angle_o = math.atan2(po[1]-circle["y"], po[0]-circle["x"])
                    angle_m = math.atan2(pm[1]-circle["y"], pm[0]-circle["x"])
                    angle_d = math.atan2(pd[1]-circle["y"], pd[0]-circle["x"])

                    # Calculate arc direction
                    if (angle_m - angle_o) % (2 * math.pi) < (angle_m - angle_d) % (2 * math.pi):
                        direction = 1
                    else:
                        direction = -1

                    # Defining offset circle
                    base_circle = circle.copy()
                    base_circle["r"] += direction * offset
                    # Circle related head dimensions
                    head_angle = direction * math.atan(head_length / base_circle["r"])
                    head_point_offset = (head_length ** 2 + base_circle["r"] ** 2) ** 0.5 - base_circle["r"]
                    # Additional half head_length offset from extremities
                    angle_o += head_angle / 2.
                    angle_d -= head_angle / 2.

                    # Circle segments
                    angle_delta = (angle_d - angle_o) % (direction * 2 * math.pi)
                    angle_delta -= head_angle

                    min_angle = max(-0.1, min(0.1, head_angle))
                    link_count = max(1, int(abs(math.ceil(angle_delta / min_angle))))
                    link_angle = angle_delta / link_count
                    angles = [angle_d - head_angle - i * link_angle for i in range(link_count + 1)]
                    
                    # Destination head part 1
                    points = [self.circle_point(base_circle, angle_d, head_point_offset)]
                    points += [self.circle_point(base_circle, angle_d - head_angle, -direction * (head_overwidth + width / 2))]
                    # Inner border
                    points += [self.circle_point(base_circle, angle, -direction * width / 2) for angle in angles]

                    # Outter border
                    points += [self.circle_point(base_circle, angle, direction * width / 2) for angle in angles[::-1]]
                    # Destination head part 2
                    points += [self.circle_point(base_circle, angle_d - head_angle, direction * (head_overwidth + width / 2))]
                    points += [self.circle_point(base_circle, angle_d, head_point_offset)]

                    # create features
                    outFeat = QgsFeature()
                    outFeat.setGeometry(QgsGeometry.fromPolygon([[QgsPoint(point[0], point[1]) for point in points]]))
                    outFeat.setAttributes([elem[1], elem[2], float(elem[0])])

                    listArrow.append(outFeat)
                    del outFeat

        provider.addFeatures(listArrow)  
        
        # Finnally commit the changes        
        layer.commitChanges()
        layer.updateExtents()
        layer.updateFields()

        return layer

    # copylayer for render3d 
    def copyLayer3d(self, base_map, useLogscale):
        ''' Return a copy of the layer with a new field with the report data '''
        
        # One row = one zone with data, the join field is in a column of the row
        report_data_mtx = self.getReportDataMtx()
        join2_field_idx = self.interface.getParameter('join2_field_idx')
        mode = self.interface.getParameter('mode')
        
        # We have one row at least
        if join2_field_idx >= len(report_data_mtx[0]):
            raise ReportError('The join field index of the report is out of range.')
        
        for row in report_data_mtx:
            self.report_data_dict[row[join2_field_idx]] = row
        
        # Join column of the layer
        join_field_idx = self.interface.getParameter('join_field_idx')
        
        mode_title = self.interface.getParameter('mode_title')
        # Selected field for the report
        joinedFields = self.getJoinedFields()

        provider_map_layer = base_map.dataProvider()

        vl = base_map
        layer = QgsVectorLayer('Polygon', vl.name()+' ('+ mode_title+')', 'memory')

        layer.startEditing()
        
        ## Copy the original layer ##
        provider = layer.dataProvider()
        
        # Copy all attributes           
        attrs = []
        for field in provider_map_layer.fields():
            attrs.append(QgsField(field.name(),field.type()))
            
        attrs.append(QgsField(BaseReport.TARGET_FIELD_NAME,QVariant.Double))
            
        res = provider.addAttributes(attrs)
        
        # Copy all features
        cfeatures=[]
        features = provider_map_layer.getFeatures()
        for feature in features:
            cfeature = QgsFeature()
            xgeometry = feature.geometry()
            cfeature_Attributes=[]
            cfeature_Attributes.extend(feature.attributes())
            
            cfeature.setGeometry(xgeometry)
            cfeature.setAttributes(cfeature_Attributes)
            cfeatures.append(cfeature)

        provider.addFeatures(cfeatures)
        
        # Finnally commit the changes
        layer.commitChanges()
        
        return layer
    
    def pointstocircle(self, a, b, c):
        """ Calculate circle center and radius from 3 points"""
        # AB and BC vectors
        abx = float(b[0] - a[0])
        aby = float(b[1] - a[1])
        bcx = float(c[0] - b[0])
        bcy = float(c[1] - b[1])
        # AB and BC middles
        ab2x = (a[0] + b[0]) / 2.
        ab2y = (a[1] + b[1]) / 2.
        bc2x = (b[0] + c[0]) / 2.
        bc2y = (b[1] + c[1]) / 2.
        
        # Aligned points
        if abs(abx * bcy - aby * bcx) < 0.0001: # Empirical threshold tested for 1E5 order of magnitude coords
        # if abx * bcy - aby * bcx == 0:
            return None
        
        # AB horizontal
        if aby == 0:
            cx = ab2x
            cy = bc2y - (cx - bc2x) * bcx / bcy
        # BC horizontal
        elif bcy == 0:
            cx = bc2x
            cy = ab2y - (cx - ab2x) * abx / aby
        # Otherwise
        else:
            cx = (bc2y - ab2y + bcx * bc2x / bcy - abx * ab2x / aby) / (bcx / bcy - abx / aby)
            cy = bc2y - (cx - bc2x) * bcx / bcy
        # Radius
        r = ((a[0] - cx) ** 2 + (a[1] - cy) ** 2) ** 0.5
        
        return {'x':cx, 'y':cy, 'r':r}

    def flowList(self, relations_data_dict, coordinatesDictionnary, number_sector):
        linesList = []
        lengthArrows = []
        for originID,values in relations_data_dict.items():
            for destinationID,value in values.items():
                if((originID != number_sector+1)and(destinationID != number_sector+1)and(originID != destinationID)):
                    originCoordinates = coordinatesDictionnary[originID]
                    destinationCoordinates = coordinatesDictionnary[destinationID]
                    linesList.append([value,originID, destinationID, originCoordinates, destinationCoordinates])
                    length = math.sqrt((destinationCoordinates[0]-originCoordinates[0])**2 + (destinationCoordinates[1]-originCoordinates[1])**2)
                    if length !=0 :
                        lengthArrows.append(length)

        return linesList,lengthArrows

    def circle_point(self, circle, angle, r_offset=0.0):
        r = circle["r"] + r_offset
        return (circle["x"] + r * math.cos(angle), circle["y"] + r * math.sin(angle))

    @classmethod
    def validatedDefaultSymbol( self, geometryType ):
        symbol = QgsSymbolV2.defaultSymbol( geometryType )
        if symbol is None:
            if geometryType == QGis.Point:
                symbol = QgsMarkerSymbolV2()
            elif geometryType == QGis.Line:
                symbol =  QgsLineSymbolV2 ()
            elif geometryType == QGis.Polygon:
                symbol = QgsFillSymbolV2 ()
        return symbol
    
    @classmethod    
    def makeSymbologyForRange( self, geometryType, min , max, title, color):
        symbol = self.validatedDefaultSymbol( geometryType )
        symbol.setColor( color )
        range = QgsRendererRangeV2( min, max, symbol, title )
        return range  
        
    @classmethod   
    def applyGraduatedSymbologyStandardMode( self, layer, field, classes, mode, color1, color2):
        symbol = self.validatedDefaultSymbol( layer.geometryType() )
        colorRamp = QgsVectorGradientColorRampV2.create({'color1':str(color1.red())+','+str(color1.green())+','+str(color1.blue())+',255', 'color2':str(color2.red())+','+str(color2.green())+','+str(color2.blue())+',255'})
        renderer = QgsGraduatedSymbolRendererV2.createRenderer( layer, field, classes, mode, symbol, colorRamp )
        layer.setRendererV2(renderer)

    @classmethod   
    def applyCategorieSymbologyStandardMode( self, layer):
        symbol = self.validatedDefaultSymbol( layer.geometryType() )
        
        # get unique values 
        field = 'FLUX'
        fni = layer.fieldNameIndex(field)
        unique_values = layer.dataProvider().uniqueValues(fni)
        unique_values.sort()
        classes = len(unique_values)

        # define categories       
        coeff = float(255.0/len(unique_values))
        color = 255
        categories = []
        for unique_value in unique_values:
            # initialize the default symbol for this geometry type
            symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())

            # configure a symbol layer
            color = color - coeff
            layer_style = {}
            if unique_value == 0:
                layer_style['color'] = '%d, %d, %d' % (255, 255, 255)
            else:
                layer_style['color'] = '%d, %d, %d' % (255, color, color)
            layer_style['outline'] = '#000000'
            symbol_layer = QgsSimpleFillSymbolLayerV2.create(layer_style)

            # replace default symbol layer with the configured one
            if symbol_layer is not None:
                symbol.changeSymbolLayer(0, symbol_layer)

            # create renderer object
            category = QgsRendererCategoryV2(unique_value, symbol, str(unique_value))
            # entry for the list of category items
            categories.append(category)

        renderer = QgsCategorizedSymbolRendererV2(field,categories)     # QgsGraduatedSymbolRendererV2.createRenderer( layer, field, classes, mode, symbol, colorRamp )
        layer.setRendererV2( renderer )
    
    @classmethod    
    def applySymbologyFixedDivisions( self, layer, field, rangeList ):   
        renderer = QgsGraduatedSymbolRendererV2( field, rangeList )
        renderer.setMode( QgsGraduatedSymbolRendererV2.Custom )
        layer.setRendererV2( renderer )   
        
        
    def generate3Drender(self, map_layer, mode):
        ''' Routine for create the 3D render version of the report '''
        
        render3d_path = self.interface.getParameter('render3dPath')+'/'+self.MODES_TITLES[mode]
        render3d_namefield = self.interface.getParameter('render3dNameField')
        rangeList = self.interface.getParameter('rangeList')
        render3d_title = self.interface.getParameter('render3dTitle')
        colorscale_start = self.interface.getParameter('colorscale_start')
        colorscale_end = self.interface.getParameter('colorscale_end')
        join_field_idx = self.interface.getParameter('join_field_idx')
        self.relations_file = self.interface.getParameter('relations_path') 
        map_fields = self.map_layer.pendingFields()
        delimiter_str = self.interface.getParameter('delimiterRelation')
        skipHeader = int(self.interface.getParameter('skipHeader'))

        i = 1
        for field in map_fields:
            if i == join_field_idx+1:
                map_id_field_str = str(field.name())
            i = i + 1            

        # Create a JSON file wich saves the relations data
        # relations_data_dict = {}
        relations_data = numpy.genfromtxt(self.relations_file, skip_header=skipHeader, dtype=None, names=True, delimiter=delimiter_str)
        number_zone = len(relations_data)-1 #the last line of relations_data is the sum total 
        
        # list of zone id
        zone_id = []
        total = {}
        row_index = 0
        for row in relations_data:
            id = row[0].split()[0]
            if id.isdigit():
                zone_id.append(int(id))
                total[id]=row[len(row)-1]
            row_index +=1

        # relations dictionary
        row_index = 0
        relations_data_dict = {}
        for row in relations_data:
            if (row_index <= 0): # number_zone-1) :
                r = int(zone_id[row_index])
                relations_data_dict[r] = {}
                column_index = 1
                while (column_index<=number_zone-1):
                    value = row[column_index]
                    c = int(zone_id[column_index-1])
                    if (isinstance(value,str)==False) & (value!=0) & (r!=c):
                        relations_data_dict[r][c] = value
                    column_index += 1
                row_index += 1    
   
        # for row in relations_data:
        #     if row[0] in relations_data_dict:
        #         relations_data_dict[int(row[0])][int(row[1])] = float(row[2])
        #     else:
        #         # We init the dictionary of relations of this zone with the rest and we insert the first value
        #         relations_data_dict[int(row[0])] = {}
        #         relations_data_dict[int(row[0])][int(row[1])] = float(row[2])
            
        #     # The symmetrical pair   
        #     if row[1] in relations_data_dict:
        #         relations_data_dict[int(row[1])][int(row[0])] = float(row[2]) 
        #     else:
        #         # We init the dictionary of relations of this zone with the rest and we insert the first value
        #         relations_data_dict[int(row[1])] = {}
        #         relations_data_dict[int(row[1])][int(row[0])] = float(row[2])

        # We translate the rangeList object to a simpler one to convert it to json

        rangeList_obj = []
        for range in rangeList:
            rangeList_obj.append( {"lowerValue": range.lowerValue(), "upperValue": range.upperValue(), "red": range.symbol().color().red(), "green": range.symbol().color().green(), "blue": range.symbol().color().blue()  } )

        file_path = render3d_path + '/data/mapdata.json'
        
        if not os.path.exists(render3d_path):
            os.makedirs(render3d_path)
            
        os.makedirs(render3d_path+'/data')
        
        if os.access(os.path.dirname(render3d_path), os.W_OK):  
            QgsVectorFileWriter.writeAsVectorFormat(map_layer, file_path, 'utf-8', map_layer.crs(), 'GeoJson') 

            # Copy main files
            shutil.copytree(os.path.dirname(os.path.abspath(__file__))+'/../templates/one_to_many/js', render3d_path+'/js')
            shutil.copyfile(os.path.dirname(os.path.abspath(__file__))+'/../templates/one_to_many/scene.htm', render3d_path+'/scene.htm')
            
            # If the user has selected to display some field of the map
            # as the name we enable it in the scene
            if render3d_namefield != '':
                render3d_namefield_enabled = True
                render3d_namefield = render3d_namefield
            else:
                render3d_namefield_enabled = False
                render3d_namefield = 'TRANUS_VAL'

                
            # Create config file with parameters
            parameters = {  
                            'zone_name_field': render3d_namefield, 
                            'zone_name_field_enabled': render3d_namefield_enabled, 
                            'range_list': rangeList_obj, 
                            'report_mode': self.MODES_TITLES[mode], 
                            'render_title': render3d_title,
                            'colorscale_start': colorscale_start,
                            'colorscale_end': colorscale_end,
                            'zone_id_field': map_id_field_str
                        }
                        
            fo = open(render3d_path + '/data/config.json', "wb")
            fo.write( json.dumps(parameters) );
            fo.close()
            
            # We save the relations data in a file
            fo = open(render3d_path + '/data/relations.json', "wb")
            fo.write( json.dumps(relations_data_dict) );
            fo.close()
            
        else:
            raise IOError("The path " + file_path + " is not writeable.")
            
            
            
    def strToQColor(self, color_str):
        ''' Transform a RGB color string to a QColor object. The format should be RRR:GGG:BBB, e.g. 255:123:000 '''
        color_rgb = str.split(color_str, ':')
        red = int(color_rgb[0])
        green = int(color_rgb[1])
        blue = int(color_rgb[2])
       
        return QtGui.QColor(red, green, blue)
        

class OneToManyReport_Dialog(QtGui.QDialog):
    ''' Class for asking for some extra input parameters for the ThematicMapReport object '''
    def __init__(self, report, parent):
        QtGui.QDialog.__init__(self, parent)
        # Set up the user interface from Designer.
        self.ui = Ui_OneToManyReportDialog()
        self.ui.setupUi(self)
        self.report = report 
        self.custom_values_templates = {}
        self.table_model = None
        
        # Selected field for the join
        self.report.interface.setParameter('join_field_idx', parent.ui.joinComboBox_Widget.currentIndex()) 
        self.report.interface.setParameter('join2_field_idx', parent.ui.join2ComboBox_Widget.currentIndex()) 
        
        # Fill the fields combo
        self.ui.render3dnameComboBox_Widget.clear()

        for field in self.report.getLayerFields():
            self.ui.render3dnameComboBox_Widget.addItem( field.name(), field )           
        
        QtCore.QObject.connect(self.ui.OKButton_Widget, QtCore.SIGNAL('clicked()'), self.renderReport)
        QtCore.QObject.connect(self.ui.cancelButton_Widget, QtCore.SIGNAL('clicked()'), self.close) 
        # Signal for toggle the column view displaying the coloer scale
        QtCore.QObject.connect(self.ui.colorModeCheckBox_Widget, QtCore.SIGNAL('clicked()'), self.toggleColorScale)    
        # Signal for toggle the column view displaying the custom values
        QtCore.QObject.connect(self.ui.customModeCheckBox_Widget, QtCore.SIGNAL('clicked()'), self.toggleValuesColumnView)
        # Signal for toggle the 3d render options
        QtCore.QObject.connect(self.ui.render3dModeCheckBox_Widget, QtCore.SIGNAL('clicked()'), self.toggleRender3dView)
        # Signal for adding a new custom value
        QtCore.QObject.connect(self.ui.addValueButton_Widget, QtCore.SIGNAL('clicked()'), self.addCustomValue)
        # Signal for remove a custom value
        QtCore.QObject.connect(self.ui.removeValueButton_Widget, QtCore.SIGNAL('clicked()'), self.removeCustomValue)
        # Signal for creating a new custom values template
        QtCore.QObject.connect(self.ui.newTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.newTemplate)
        # Signal for deleting a custom values template
        QtCore.QObject.connect(self.ui.deleteTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.deleteTemplate)
        # Signal for loading a custom values template
        QtCore.QObject.connect(self.ui.templatesComboBox_Widget, QtCore.SIGNAL('currentIndexChanged(const QString&)'), self.loadTemplate)
        # Signal for saving a custom values template
        QtCore.QObject.connect(self.ui.saveTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.saveTemplate)
        # Connect the browse 3d path with the button
        QtCore.QObject.connect(self.ui.render3dpushButton_Widget, QtCore.SIGNAL('clicked()'), self.selectRender3dPath_Action)
        # 
        QtCore.QObject.connect(self.ui.relationsPushButton_Widget, QtCore.SIGNAL('clicked()'), self.selectRelationsFile_Action)
        

        # We set the column view widget
        self.table_model = QtGui.QStandardItemModel(0, 3, self.ui.valuesTableView_Widget)
        header_labels = ['Base value', 'Top value', 'Label', 'Color']
        self.table_model.setHorizontalHeaderLabels(header_labels)
        self.ui.valuesTableView_Widget.setModel(self.table_model)  

        QtCore.QObject.connect(self.ui.valuesTableView_Widget, QtCore.SIGNAL("clicked(QModelIndex)"), self.colorSelect_Action)

        # Initialize the templates combo
        self.initTemplatesCombo()
        
        # Change the style of the color pushbuttons
        self.report.interface.setParameter('colorscale_start', '255:255:255') 
        self.report.interface.setParameter('colorscale_end', '255:0:0') 
        
        QtCore.QObject.connect(self.ui.color1Button_Widget, QtCore.SIGNAL('clicked()'), self.changeGradientStart_Action)
        QtCore.QObject.connect(self.ui.color2Button_Widget, QtCore.SIGNAL('clicked()'), self.changeGradientEnd_Action)

    def renderReport(self):
        ''' Validation function that calls the real renderReport function if everything is OK '''
        
        self.report.interface.setParameter('useLogscale', False) 
        self.report.interface.setParameter('render3dPath', self.ui.render3dpathLineEdit_Widget.text())
        self.report.interface.setParameter('render3dNameField', self.ui.render3dnameComboBox_Widget.currentText())
        self.report.interface.setParameter('render3dTitle', self.ui.render3dtitleLineEdit_Widget.text())
        self.report.interface.setParameter('relations_path', self.ui.relationsFilePathLineEdit_Widget.text())
        self.report.interface.setParameter('skipHeader', self.ui.skipHeader_Widget.text())
        self.report.interface.setParameter('delimiterRelation', self.ui.delimiterRelation_Widget.text())
        self.report.interface.setParameter('factorArrow', self.ui.factor_Widget.text())

        ### We set the modes to be rendered ###
        modes = {}
        if self.ui.equalIntervalModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.EqualInterval] = "Equal Interval"
        if self.ui.customModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.Custom] = "Custom"   

        if self.ui.render3dModeCheckBox_Widget.isChecked():
            self.report.interface.setParameter('render3d', True)  
        else:
            self.report.interface.setParameter('render3d', False)  
            
        if self.ui.render2dModeCheckBox_Widget.isChecked():
            self.report.interface.setParameter('render2d', True)  
        else:
            self.report.interface.setParameter('render2d', False)   

        # Validations
        if self.ui.relationsFilePathLineEdit_Widget.text() == "":
            msgBox = QMessageBox.warning(self, "Error", "You have to select a relations data file.")
        elif modes.keys() == []:
            msgBox = QMessageBox.warning(self, "Error", "At least one mode has to be selected.")
        elif self.ui.customModeCheckBox_Widget.isChecked() and self.table_model.rowCount() == 0:
            msgBox = QMessageBox.warning(self, "Error", "At least one value has to be created if you select Custom mode.")
        else:
            for mode in modes.keys():
                # Finally we render the report
                self.report.interface.setParameter('mode', mode)
                self.report.interface.setParameter('mode_title', modes[mode])
 
                #layer = self.report.copyLayer(self.report.map_layer)
                layer = self.report.map_layer
                
                ### We set the range list of the custom mode parameter ###
                rangeList = []
                # Model of the tableview with the range data
                model = self.table_model
                for row in range(model.rowCount()):
                    data = []
                    for column in range(model.columnCount()):
                        index = model.index(row, column)
                        # We suppose data are strings
                        data.append(str(model.data(index)))
                    
                    bottomValue = float(data[0])
                    topValue = float(data[1])
                    label = str(data[2])
                    color = self.report.strToQColor(str(data[3]))
                    
                    # lambda function for random color generation
                    rangeList.append( self.report.makeSymbologyForRange( layer.geometryType(), bottomValue, topValue, label, color ) )  
                    
                self.report.interface.setParameter('rangeList', rangeList)
                
                self.report.renderReport(layer)
                
            self.close() 

    def toggleColorScale(self):
        # Render par corlor or not
        if self.ui.colorModeCheckBox_Widget.isChecked():
            self.report.interface.setParameter('colorMode', True)  
            self.ui.color1Button_Widget.setEnabled(True)
            self.ui.color2Button_Widget.setEnabled(True)
            self.ui.colorScaleLabel_Widget.setEnabled(True)
            self.ui.color1Button_Widget.setStyleSheet("background-color:#FFFFFF; border: 1px solid #000000")
            self.ui.color2Button_Widget.setStyleSheet("background-color:#FF0000; border: 1px solid #000000")
        
        else:
            self.report.interface.setParameter('colorMode', False)  
            self.ui.color1Button_Widget.setEnabled(False)
            self.ui.color2Button_Widget.setEnabled(False)
            self.ui.colorScaleLabel_Widget.setEnabled(False)
            self.ui.color1Button_Widget.setStyleSheet("background-color:none; border: 1px solid #000000")
            self.ui.color2Button_Widget.setStyleSheet("background-color:none; border: 1px solid #000000")
        
    def toggleValuesColumnView(self):
        ''' This method enable and disable widgets in case we need the custom value mode '''
        if self.ui.customModeCheckBox_Widget.isChecked():
            self.ui.customValuesLabel_Widget.setEnabled(True)
            self.ui.valuesTableView_Widget.setEnabled(True)
            self.ui.addValueButton_Widget.setEnabled(True)
            self.ui.removeValueButton_Widget.setEnabled(True)
            self.ui.newTemplateButton_Widget.setEnabled(True)
            self.ui.saveTemplateButton_Widget.setEnabled(True)
            self.ui.templatesComboBox_Widget.setEnabled(True)
            self.ui.deleteTemplateButton_Widget.setEnabled(True)
        else:
            self.ui.customValuesLabel_Widget.setEnabled(False)
            self.ui.valuesTableView_Widget.setEnabled(False)
            self.ui.addValueButton_Widget.setEnabled(False)
            self.ui.removeValueButton_Widget.setEnabled(False)
            self.ui.newTemplateButton_Widget.setEnabled(False)
            self.ui.saveTemplateButton_Widget.setEnabled(False)
            self.ui.templatesComboBox_Widget.setEnabled(False)
            self.ui.deleteTemplateButton_Widget.setEnabled(False)
    
    def toggleRender3dView(self):
        ''' This method enable and disable widgets in case we need the custom value mode '''
        if self.ui.render3dModeCheckBox_Widget.isChecked():
            self.ui.render3dLabel_Widget.setEnabled(True)
            self.ui.render3dpathLabel_Widget.setEnabled(True)
            self.ui.render3dpathLineEdit_Widget.setEnabled(True)
            self.ui.render3dpushButton_Widget.setEnabled(True)
            self.ui.render3dnameLabel_Widget.setEnabled(True)
            self.ui.render3dnameComboBox_Widget.setEnabled(True)
            self.ui.render3dtitleLineEdit_Widget.setEnabled(True)
            self.ui.render3dtitleLabel_Widget.setEnabled(True)
        else:
            self.ui.render3dLabel_Widget.setEnabled(False)
            self.ui.render3dpathLabel_Widget.setEnabled(False)
            self.ui.render3dpathLineEdit_Widget.setEnabled(False)
            self.ui.render3dpushButton_Widget.setEnabled(False)
            self.ui.render3dnameLabel_Widget.setEnabled(False)
            self.ui.render3dnameComboBox_Widget.setEnabled(False)
            self.ui.render3dtitleLineEdit_Widget.setEnabled(False)
            self.ui.render3dtitleLabel_Widget.setEnabled(False)
            
    def selectRender3dPath_Action(self):
        ''' Display browse file dialog '''
        fname = QtGui.QFileDialog.getExistingDirectory(self, 'Select directory')
        if fname:
            self.ui.render3dpathLineEdit_Widget.setText(fname)
            
    def selectRelationsFile_Action(self):
        ''' Display browse file dialog '''
        fname = QtGui.QFileDialog.getOpenFileName(self, 'Select directory')
        if fname:
            self.ui.relationsFilePathLineEdit_Widget.setText(fname)
    
    def colorSelect_Action(self, qmodelindex):
    
        col_index = qmodelindex.column()

        if col_index == self.table_model.columnCount() - 1:
           
            color = color = self.report.strToQColor(str(qmodelindex.data(QtCore.Qt.DisplayRole)))
            col = QtGui.QColorDialog.getColor(color , self)

            if col.isValid():
                self.table_model.setData(qmodelindex, str(col.red())+":"+str(col.green())+":"+str(col.blue()))
                std_item = self.table_model.item(qmodelindex.row(), qmodelindex.column())
                brush = QtGui.QBrush(QtGui.QColor(col.red(), col.green(), col.blue()))
                std_item.setBackground(brush)
    
    def changeGradientStart_Action(self):
    
        color_str = self.report.interface.getParameter('colorscale_start')
        color = self.report.strToQColor(color_str)
        col = QtGui.QColorDialog.getColor(color , self)

        if col.isValid():
            self.ui.color1Button_Widget.setStyleSheet("background-color:rgb("+str(col.red())+","+str(col.green())+","+str(col.blue())+"); border: 1px solid #000000")
            self.report.interface.setParameter('colorscale_start',str(col.red())+":"+str(col.green())+":"+str(col.blue()))
            
    def changeGradientEnd_Action(self):
    
        color_str = self.report.interface.getParameter('colorscale_end')
        color = self.report.strToQColor(color_str)
        col = QtGui.QColorDialog.getColor(color , self)

        if col.isValid():
            self.ui.color2Button_Widget.setStyleSheet("background-color:rgb("+str(col.red())+","+str(col.green())+","+str(col.blue())+"); border: 1px solid #000000")
            self.report.interface.setParameter('colorscale_end',str(col.red())+":"+str(col.green())+":"+str(col.blue()))
        
    def clearTableView(self):
        self.table_model.removeRows(0, self.table_model.rowCount())
        
    def addCustomValue(self):
        ''' Add a new custom value to the table view model '''
        r = lambda: random.randint(0,255)
        red = 255
        green = 0
        blue = 0
        color_item = QStandardItem(str(red)+":"+str(green)+":"+str(blue))
        brush = QtGui.QBrush(QtGui.QColor(red, green, blue))
        brush.setStyle(QtCore.Qt.SolidPattern)
        color_item.setBackground(brush)
        data = [QStandardItem("0"), QStandardItem("0"), QStandardItem("0 - 0"), color_item]
        
        self.table_model.appendRow(data)
        
    def loadTemplate(self):
        current_index = self.ui.templatesComboBox_Widget.currentText()
        if current_index in self.custom_values_templates:
            self.clearTableView()
            data = self.custom_values_templates[current_index]
            
            for row in data:
                item = []
                
                col_number = 0
                for col in row:
                    # If is the last column we fill with the color
                    std_item = None
                    if col_number == len(row)-1:
                        std_item = QStandardItem(col)
                        color = self.report.strToQColor(str(col))
                        brush = QtGui.QBrush(color)
                        brush.setStyle(QtCore.Qt.SolidPattern)
                        std_item.setBackground(brush)
                    else:
                        std_item = QStandardItem(col)
                    
                    item.append(std_item)
                    
                    col_number = col_number + 1
                
                self.table_model.appendRow(item)
        else:
            self.clearTableView()
            
        
        
    def saveTemplate(self):
        current_index = self.ui.templatesComboBox_Widget.currentText()
        if current_index in self.custom_values_templates:
            # Model of the tableview with the range data
            rows = []
            for row in range(self.table_model.rowCount()):
                cols = []
                for column in range(self.table_model.columnCount()):
                    index = self.table_model.index(row, column)
                    # We suppose data are strings
                    cols.append(str(self.table_model.data(index)))
                rows.append(cols)
            self.custom_values_templates[self.ui.templatesComboBox_Widget.currentText()] = rows
            self.saveCurrentTemplates()
            
    def newTemplate(self):
        # Display dialog box asking for the template name
        self.prompt_dlg = ThematicMapReportPrompt_Dialog(self)
        if self.prompt_dlg.exec_():
            name = self.prompt_dlg.name
            self.prompt_dlg.close()
            # Save the new template to settings
            self.custom_values_templates[name] = []
            self.saveCurrentTemplates()
            # Show it in the combo
            self.ui.templatesComboBox_Widget.addItem(name, name)
            self.ui.templatesComboBox_Widget.setCurrentIndex(self.ui.templatesComboBox_Widget.count()-1)
            self.clearTableView()
            
    def deleteTemplate(self):
        current_template_index = self.ui.templatesComboBox_Widget.currentIndex()
        current_template = self.ui.templatesComboBox_Widget.currentText()
        if current_template in self.custom_values_templates:
            self.custom_values_templates.pop(current_template)
            self.ui.templatesComboBox_Widget.removeItem(current_template_index)
            # Save new config
            self.saveCurrentTemplates()
        
    def removeCustomValue(self):
        indexes = self.ui.valuesTableView_Widget.selectedIndexes()
        for index in indexes:
            self.table_model.removeRow(index.row())
            
            
    def initTemplatesCombo(self):
        # We get the custom values templates saved in the INI file
        self.custom_values_templates = self.report.settings.value("thematic/custom_values_templates", {} )
        # Fill the combo with them
        self.ui.templatesComboBox_Widget.addItem("", "")
        for key in self.custom_values_templates.keys():
            self.ui.templatesComboBox_Widget.addItem(key, key)
            
    def saveCurrentTemplates(self):
        self.report.settings.setValue("thematic/custom_values_templates", self.custom_values_templates)      

class OneToManyReportPrompt_Dialog(QtGui.QDialog):
    ''' Class for asking for the custom values template name '''
    def __init__(self, parent):
        QtGui.QDialog.__init__(self, parent)
        # Set up the user interface from Designer.
        self.ui = Ui_ThematicMapReportPromptDialog()
        self.ui.setupUi(self)
        self.parent = parent
        self.name = ""
        
        # Signal for saving the name
        QtCore.QObject.connect(self.ui.saveButton_Widget, QtCore.SIGNAL('clicked()'), self.loadName)
        QtCore.QObject.connect(self.ui.cancelButton_Widget, QtCore.SIGNAL('clicked()'), self.reject)
        
    def loadName(self):
        name = self.ui.nameLineEdit_Widget.text()
        if name == "":
            msgBox = QMessageBox.warning(self, "Error", "The name cannot be empty.")
        elif name in self.parent.custom_values_templates:
            msgBox = QMessageBox.warning(self, "Error", "That name is already in use.")
        else:
            self.name = self.ui.nameLineEdit_Widget.text()
            self.accept()
    
    def getName(self):
        return self.name