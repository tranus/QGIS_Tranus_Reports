from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from qgis.gui import *

import numpy

class BaseReport:
    """ Base class that represents the report files imported 
    
    Attributes:
      qgis_iface (QgisInterface): Class for interface with QGIS
      report_file (str): The path to the raw/binary file witch holds the report.     
      map_layer (QgsVectorLayer): Selected vector layer to render the results
      parent_widget (QWidget): Parent widget which loaded the report
    """
    __valid_report_types = [{"class_name": "ThematicMapReport", "module_name": "thematic_report", "label" : "Thematic map"}, {"class_name": "OneToManyReport", "module_name": "one_to_many_report", "label" : "One to Many comparison"}]
    TARGET_FIELD_NAME = "TRANUS_VAL"
    
    def __init__(self, interface=None, report_file=None, map_layer=None, parent_widget=None):
        self.report_file = report_file
        self.f_report_file = None # Report file handler
        self.map_layer = map_layer
        self.report_loaded = False
        self.parent_widget = parent_widget
        self.file_delimiter = ',' # Report file CSV delimiter
        self.report_data_mtx = None # Report matrix data
        self.zones_axis = 'row' # Axis containing the TRANUS zones 
        self.report_data_dict = {} # Dictionary of the report data indexed by TRANUS zone ID 
        self.config_dlg = None # Dialog object for extra parameters
        self.interface = interface # Pointer to the interface object

    @classmethod
    def getReportTypes(cls):
        ''' Return a list of all the available report types '''
        return cls.__valid_report_types
        
    def getLayerFields(self):
        ''' Return the fields/attributes of the selected map layer '''
        return self.map_layer.pendingFields()
        
    def getReportDataMtx(self):
        ''' Return the parsed report file in a matrix object '''
        return self.report_data_mtx
            
    def isLoaded(self):
        ''' Return true if the report file is loaded and initialised or false otherwise '''
        return self.report_loaded
        
    def processReportFile(self):
        ''' Process and clean the report file to get a data matrix '''
        self.report_data_mtx = numpy.genfromtxt(self.f_report_file, skip_header=0, delimiter=self.file_delimiter, dtype=None, names=True) 
        # We rotate 90 degrees counter-clockwise the matrix if the zone ID is not in a column
        if self.zones_axis == 'column':
            self.report_data_mtx = numpy.rot90(self.report_data_mtx)

    def getJoinedFields(self):        
        ''' Return a list of the possible columns of the report, used to join with a map layer field '''
        fields = enumerate(self.report_data_mtx.dtype.names)  
        
        result = []
        for field in fields:
            result.append({'value': field[0], 'label': str(field[1]) })
            
        return result

    def getSectorFields(self):

        ''' Return a list of sector , used to create the report '''
        fields = enumerate(numpy.unique(self.report_data_mtx['Sector']))
        
        result = []
        for field in fields:
            result.append({'value': field[0], 'label': str(field[1]) })   

        # numpy.savetxt('C:/Users/TieuMy/Desktop/test.txt',self.report_data_mtx[self.report_data_mtx['Sector']=='1 BasicEmp'],fmt="%s") 

        return result

        
    def load(self, report_delimiter=',', zones_axis='row'):
        ''' This method build the report processing the necessary data '''
        
        try:
            self.f_report_file = open(self.report_file, 'r')
        except Exception as e:
            raise ReportError('Failed to open Matrix data file: '+self.report_file)
            
        self.file_delimiter = report_delimiter
        self.zones_axis = zones_axis
        
        self.processReportFile()
        
        try:
            if len(self.report_data_mtx)>0 and len(self.report_data_mtx[0])>0:
                self.report_loaded = True
            else:
                raise ReportError('The report file is empty: '+self.report_file)
        except Exception as e:
            raise ReportError('Failed to parse Matrix data file, try a different delimiter.')

  
    def createDictionnary(self, inputLayer, index):  # creates a dictionnary of coordinates
        ''' 
            Extracts the coordinates of the centroids of the features from a layer and creates a dictionnary : ID -> (x,y). 
        '''
        coordinatesDictionnary = {}

        # Only selected features 
        if inputLayer.selectedFeatures():
            features = inputLayer.selectedFeatures()
        else:
            features = inputLayer.getFeatures()

        for elem in features:
            centroid = elem.geometry().centroid().asPoint()
            IDvalue = elem.attributes()[index]
            coordinatesDictionnary[IDvalue] = centroid

        return coordinatesDictionnary

class ReportError(Exception):
    pass