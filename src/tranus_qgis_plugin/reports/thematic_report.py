from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from qgis.core import *
from qgis.gui import *

from thematic_map_report_ui import Ui_ThematicMapReportDialog
from thematic_map_report_prompt_ui import Ui_ThematicMapReportPromptDialog

from base_report import *
from random import randrange

import random, math, os, shutil, sys, json, re

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'\..')

class ThematicMapReport(BaseReport):
    ''' This report inherit from the base report class. This kind of report render a graduated map using the data from one field of the report.
        Each polygon/area in the map layer will be coloured according to his value in the report file.
    '''
    MODES_TITLES = {QgsGraduatedSymbolRendererV2.EqualInterval: "Equal Interval", QgsGraduatedSymbolRendererV2.Quantile: "Quantile", QgsGraduatedSymbolRendererV2.Jenks: "Natural Breaks", QgsGraduatedSymbolRendererV2.StdDev: "Standard Deviation", QgsGraduatedSymbolRendererV2.Pretty: "Pretty Breaks", QgsGraduatedSymbolRendererV2.Custom: "Custom"}

    def __init__(self, report_file=None, map_layer=None, parent_widget=None):  
        report_data_mtx = []
        # Create a dict for indexing the report by joined fields
        self.report_data_dict = {}   
        self.settings = QSettings("inria", "tranusreports")
  
    # def get_num(x):
    #     return float(''.join(ele for ele in x if ele.isdigit() or ele == '.'))

    def renderReport(self, layer):
        ''' Render the report in the map layer '''

        # Selected graduated modes
        mode = self.interface.getParameter('mode')
        rangeList = self.interface.getParameter('rangeList')

        render3d = self.interface.getParameter('render3d')
        render2d = self.interface.getParameter('render2d')
        color1 = self.strToQColor(self.interface.getParameter('colorscale_start'))
        color2 = self.strToQColor(self.interface.getParameter('colorscale_end'))
        
        layer_2d, number_zone = self.copyLayer(layer, useLogscale=self.interface.getParameter('useLogscale') )

        # Number of classes of the gradient
        autoClass = self.interface.getParameter('autoClassification')
        if(autoClass):
            classes = number_zone
        else:
            classes = self.interface.getParameter('classes')

        if classes < 1:
            raise ReportError('There must be at least one class division.')
        
        if render2d:
            # Apply graduated symbology for this layer
            if layer_2d.isValid():
                # If we selected Custom mode we render an extra layer with this values
                if mode == QgsGraduatedSymbolRendererV2.Custom:
                    if len(rangeList)>0: 
                        self.applySymbologyFixedDivisions( layer_2d, BaseReport.TARGET_FIELD_NAME, rangeList )
                    else:
                        raise ReportError('In Custom mode the range list cannot be empty.')
                else:
                    if (autoClass):
                        self.applyCategorieSymbologyStandardMode( layer_2d, BaseReport.TARGET_FIELD_NAME , classes, mode, color1, color2)
                    else:    
                        self.applyGraduatedSymbologyStandardMode( layer_2d, BaseReport.TARGET_FIELD_NAME , classes, mode, color1, color2)
                
                # label = QgsPalLayerSettings()
                # label.readFromLayer(layer_2d)
                # label.enabled = True
                # label.isExpression = True
                # label.fieldName = 'if(TRANUS_VAL>0 , TRANUS_VAL, "")'
                # label.placement= QgsPalLayerSettings.AroundPoint
                # label.setDataDefinedProperty(QgsPalLayerSettings.Size,True,True,'8','')
                # label.writeToLayer(layer_2d)

                QgsMapLayerRegistry.instance().addMapLayers( [layer_2d] ) 
        
        layer_3d = None
        if render3d:  
            if mode == QgsGraduatedSymbolRendererV2.EqualInterval or mode == QgsGraduatedSymbolRendererV2.Custom:
                layer_3d, number_zone  = self.copyLayer(layer, useLogscale=False)
                if layer_3d.isValid():
                    self.generate3Drender(layer_3d, mode)
            else:
                print "Ignored flag --render3d, is only valid for Custom and EqualInterval"
                
        return {'layer2d': layer_2d, 'layer3d': layer_3d}
                
        
    def copyLayer(self, base_map, useLogscale):
        ''' Return a copy of the layer with a new field with the report data '''
        # One row = one zone with data, the join field is in a column of the row
        report_data_mtx = self.getReportDataMtx()
        join2_field_idx = self.interface.getParameter('join2_field_idx')
        density = self.interface.getParameter('density')

        mode = self.interface.getParameter('mode')
        
        # We have one row at least
        if join2_field_idx >= len(report_data_mtx[0]):
            raise ReportError('The join field index of the report is out of range.')

         # Selected field for the report
        targetSector = self.interface.getParameter('targetSector')
        targetField  = self.interface.getParameter('targetField')

        # We have to convert data fields from string to number 
        number_zone = 0;
        for row in report_data_mtx[report_data_mtx['Sector']==targetSector]:     
            data = ()  
            number_zone += 1
            for s in row:
                if (isinstance(s, str)):
                    value = s.split()[0]
                    if value.isdigit():
                        data += (float(value),)
                    else:
                        data += (value,)     #(float(''.join(ele for ele in s if ele.isdigit() or ele == '.')),)
                else:
                    data += (s,)
            self.report_data_dict[data[join2_field_idx]] = data

        # Join column of the layer
        join_field_idx = self.interface.getParameter('join_field_idx')
        
        mode_title = self.interface.getParameter('mode_title')

        # Selected field for the report
        targetField_index = self.interface.getParameter('targetField_index')

        joinedFields = self.getJoinedFields()
        
        if targetField_index < len(joinedFields):
            targetField = joinedFields[targetField_index]
        else:
            raise ReportError('The value field index of the report is out of range.')

        provider_map_layer = base_map.dataProvider()

        vl = base_map
        layer = QgsVectorLayer('Polygon', vl.name()+' ('+ mode_title+')', 'memory')


        layer.startEditing()

        ## Copy the original layer ##
        provider = layer.dataProvider()
        
        # Copy all attributes           
        attrs = []
        for field in provider_map_layer.fields():
            attrs.append(QgsField(field.name(),field.type()))
            
        attrs.append(QgsField(BaseReport.TARGET_FIELD_NAME,QVariant.Double))
            
        res = provider.addAttributes(attrs)
        
        # Copy all features
        cfeatures=[]
        features = provider_map_layer.getFeatures()
        for feature in features:
            cfeature = QgsFeature()
            xgeometry = feature.geometry()
            cfeature_Attributes=[]
            cfeature_Attributes.extend(feature.attributes())
            
            # Surface - Area
            area = xgeometry.area()

            ## Add the report value ##
            # First we index in the layer attributes using the join combo to obtain the index value in the report
            
            if join_field_idx < len(cfeature_Attributes):
                report_index_value = cfeature_Attributes[join_field_idx]
            else:
                raise ReportError('The join field index of the map is out of range.')
            
            if type(report_index_value) is QVariant:
                report_index_value = cfeature_Attributes[join_field_idx].toFloat()[0] 

            if report_index_value in self.report_data_dict:
                # This variable holds the zone data
                feature_report_values = self.report_data_dict[report_index_value]
                # Now we get the desired value to be used in the report
                if (density):                    
                    report_value = feature_report_values[targetField_index]/area
                else:
                    report_value = feature_report_values[targetField_index]

                if useLogscale and not mode == QgsGraduatedSymbolRendererV2.Custom: 
                    if report_value > 1:
                        report_value = math.log10(feature_report_values[targetField_index])
                    else:
                        report_value = 1
                        
            else:
                report_value = 0
                
            cfeature_Attributes.append(float(report_value))
            cfeature.setGeometry(xgeometry)
            cfeature.setAttributes(cfeature_Attributes)
            cfeatures.append(cfeature)

        provider.addFeatures(cfeatures)

        # Finnally commit the changes        
        layer.commitChanges()
        
        layer.setLayerName("Sector: %s - Field: %s " %(targetSector,targetField['label']) )

        return layer, number_zone
                    

    def flowList(self, relations_data_dict, coordinatesDictionnary, number_sector):
        linesList = []
        for originID,values in relations_data_dict.items():
            for destinationID,value in values.items():
                if((originID != number_sector+1)and(destinationID != number_sector+1)):
                    originCoordinates = coordinatesDictionnary[originID]
                    destinationCoordinates = coordinatesDictionnary[destinationID]
                    linesList.append([value,originID, destinationID, originCoordinates, destinationCoordinates])

        return linesList
                

    @classmethod
    def validatedDefaultSymbol( self, geometryType ):
        symbol = QgsSymbolV2.defaultSymbol( geometryType )
        if symbol is None:
            if geometryType == QGis.Point:
                symbol = QgsMarkerSymbolV2()
            elif geometryType == QGis.Line:
                symbol = QgsLineSymbolV2 ()
            elif geometryType == QGis.Polygon:
                symbol = QgsFillSymbolV2 ()
        return symbol
    
    @classmethod    
    def makeSymbologyForRange( self, geometryType, min , max, title, color):
        symbol = self.validatedDefaultSymbol( geometryType )
        symbol.setColor( color )
        range = QgsRendererRangeV2( min, max, symbol, title )
        return range  
    
    @classmethod   
    def applyGraduatedSymbologyStandardMode( self, layer, field, classes, mode, color1, color2):
        symbol = self.validatedDefaultSymbol( layer.geometryType() )
        colorRamp = QgsVectorGradientColorRampV2.create({'color1':str(255)+','+str(255)+','+str(255)+',255', 'color2':str(color2.red())+','+str(color2.green())+','+str(color2.blue())+',255'})
        renderer = QgsGraduatedSymbolRendererV2.createRenderer( layer, field, classes, mode, symbol, colorRamp )
        layer.setRendererV2( renderer )

    @classmethod   
    def applyCategorieSymbologyStandardMode( self, layer, field, classes, mode, color1, color2):
        symbol = self.validatedDefaultSymbol( layer.geometryType() )
        colorRamp = QgsVectorGradientColorRampV2.create({'color1':str(255)+','+str(255)+','+str(255)+',255', 'color2':str(color2.red())+','+str(color2.green())+','+str(color2.blue())+',255'})
        
        # get unique values 
        fni = layer.fieldNameIndex('TRANUS_VAL')
        unique_values = layer.dataProvider().uniqueValues(fni)
        unique_values.sort()

        # define categories       
        coeff = float(255.0/len(unique_values))
        color = 255
        categories = []
        for unique_value in unique_values:
            # initialize the default symbol for this geometry type
            symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())

            # configure a symbol layer
            color = color - coeff
            layer_style = {}
            if unique_value == 0:
                layer_style['color'] = '%d, %d, %d' % (255, 255, 255)
            else:
                layer_style['color'] = '%d, %d, %d' % (255, color, color)
            layer_style['outline'] = '#000000'
            symbol_layer = QgsSimpleFillSymbolLayerV2.create(layer_style)

            # replace default symbol layer with the configured one
            if symbol_layer is not None:
                symbol.changeSymbolLayer(0, symbol_layer)

            # create renderer object
            category = QgsRendererCategoryV2(unique_value, symbol, str(unique_value))
            # entry for the list of category items
            categories.append(category)

        renderer = QgsCategorizedSymbolRendererV2(field,categories)     # QgsGraduatedSymbolRendererV2.createRenderer( layer, field, classes, mode, symbol, colorRamp )
        #renderer.setSourceColorRamp(colorRamp)
        layer.setRendererV2( renderer )
    
    @classmethod    
    def applySymbologyFixedDivisions( self, layer, field, rangeList ):   
        renderer = QgsGraduatedSymbolRendererV2( field, rangeList )
        renderer.setMode( QgsGraduatedSymbolRendererV2.Custom )
        layer.setRendererV2( renderer )   
        

    def generate3Drender(self, map_layer, mode):
        ''' Routine for create the 3D render version of the report '''

        render3d_title = self.interface.getParameter('render3dTitle')
        render3d_path = self.interface.getParameter('render3dPath')+'/'+render3d_title+'_'+self.MODES_TITLES[mode]+"test2"
        render3d_namefield = self.interface.getParameter('render3dNameField')
        rangeList = self.interface.getParameter('rangeList')
        colorscale_start = self.interface.getParameter('colorscale_start')
        colorscale_end = self.interface.getParameter('colorscale_end')
        
        # We translate the rangeList object to a simpler one to convert it to json
        rangeList_obj = []
        for range in rangeList:
            rangeList_obj.append( {"lowerValue": range.lowerValue(), "upperValue": range.upperValue(), "red": range.symbol().color().red(), "green": range.symbol().color().green(), "blue": range.symbol().color().blue()  } )

        file_path = render3d_path + '/data/mapdata.json'
        
        if not os.path.exists(render3d_path):
            os.makedirs(render3d_path)
            
        os.makedirs(render3d_path+'/data')
        
        if os.access(os.path.dirname(render3d_path), os.W_OK):  
            QgsVectorFileWriter.writeAsVectorFormat(map_layer, file_path, 'utf-8', map_layer.crs(), 'GeoJson') 

            # Copy main files
            shutil.copytree(os.path.dirname(os.path.abspath(__file__))+'/../templates/thematic_map/js', render3d_path+'/js')
            shutil.copyfile(os.path.dirname(os.path.abspath(__file__))+'/../templates/thematic_map/scene.htm', render3d_path+'/scene.htm')
            
            # If the user has selected to display some field of the map
            # as the name we enable it in the scene
            if render3d_namefield != '':
                render3d_namefield_enabled = True
                render3d_namefield = render3d_namefield
            else:
                render3d_namefield_enabled = False
                render3d_namefield = 'TRANUS_VAL'

                
            # Create config file with parameters
            parameters = {  
                            'zone_name_field': render3d_namefield, 
                            'zone_name_field_enabled': render3d_namefield_enabled, 
                            'range_list': rangeList_obj, 
                            'report_mode': self.MODES_TITLES[mode], 
                            'render_title': render3d_title,
                            'colorscale_start': colorscale_start,
                            'colorscale_end': colorscale_end,
                        }
                        
            fo = open(render3d_path + '/data/config.json', "wb")
            fo.write( json.dumps(parameters) );
            fo.close()
            
        else:
            raise IOError("The path " + file_path + " is not writeable.")
            
            
            
    def strToQColor(self, color_str):
        ''' Transform a RGB color string to a QColor object. The format should be RRR:GGG:BBB, e.g. 255:123:000 '''
        color_rgb = str.split(color_str, ':')
        red = int(color_rgb[0])
        green = int(color_rgb[1])
        blue = int(color_rgb[2])
       
        return QtGui.QColor(red, green, blue)
        

class ThematicMapReport_Dialog(QtGui.QDialog):
    ''' Class for asking for some extra input parameters for the ThematicMapReport object '''
    def __init__(self, report, parent):
        QtGui.QDialog.__init__(self, parent)
        # Set up the user interface from Designer.
        self.ui = Ui_ThematicMapReportDialog()
        self.ui.setupUi(self)
        self.report = report 
        self.custom_values_templates = {}
        self.table_model = None
        
        # Selected field for the join
        self.report.interface.setParameter('join_field_name', parent.ui.joinComboBox_Widget.currentText()) 
        self.report.interface.setParameter('join_field_idx', parent.ui.joinComboBox_Widget.currentIndex()) 
        self.report.interface.setParameter('join2_field_idx', parent.ui.join2ComboBox_Widget.currentIndex()) 
        
        # Fill the fields combo
        self.ui.featuredFieldcomboBox_Widget.clear()
        self.ui.featuredSectorcomboBox_Widget.clear()
        self.ui.render3dnameComboBox_Widget.clear()

        
        for field in self.report.getJoinedFields():
            self.ui.featuredFieldcomboBox_Widget.addItem( field['label'] , field['value'] )

        for field in self.report.getSectorFields():
            self.ui.featuredSectorcomboBox_Widget.addItem( field['label'] , field['value'] )
        
        for field in self.report.getLayerFields():
            self.ui.render3dnameComboBox_Widget.addItem( field.name(), field )           
        
        QtCore.QObject.connect(self.ui.OKButton_Widget, QtCore.SIGNAL('clicked()'), self.renderReport)
        QtCore.QObject.connect(self.ui.cancelButton_Widget, QtCore.SIGNAL('clicked()'), self.close)     
        # Signal for toggle the column view displaying the custom values
        QtCore.QObject.connect(self.ui.customModeCheckBox_Widget, QtCore.SIGNAL('clicked()'), self.toggleValuesColumnView)
        # Signal for toggle the 3d render options
        QtCore.QObject.connect(self.ui.render3dModeCheckBox_Widget, QtCore.SIGNAL('clicked()'), self.toggleRender3dView)
        # Signal for adding a new custom value
        QtCore.QObject.connect(self.ui.addValueButton_Widget, QtCore.SIGNAL('clicked()'), self.addCustomValue)
        # Signal for remove a custom value
        QtCore.QObject.connect(self.ui.removeValueButton_Widget, QtCore.SIGNAL('clicked()'), self.removeCustomValue)
        # Signal for creating a new custom values template
        QtCore.QObject.connect(self.ui.newTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.newTemplate)
        # Signal for deleting a custom values template
        QtCore.QObject.connect(self.ui.deleteTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.deleteTemplate)
        # Signal for loading a custom values template
        QtCore.QObject.connect(self.ui.templatesComboBox_Widget, QtCore.SIGNAL('currentIndexChanged(const QString&)'), self.loadTemplate)
        # Signal for saving a custom values template
        QtCore.QObject.connect(self.ui.saveTemplateButton_Widget, QtCore.SIGNAL('clicked()'), self.saveTemplate)
        # Connect the browse 3d path with the button
        QtCore.QObject.connect(self.ui.render3dpushButton_Widget, QtCore.SIGNAL('clicked()'), self.selectRender3dPath_Action)

        # We set the column view widget
        self.table_model = QtGui.QStandardItemModel(0, 3, self.ui.valuesTableView_Widget)
        header_labels = ['Base value', 'Top value', 'Label', 'Color']
        self.table_model.setHorizontalHeaderLabels(header_labels)
        self.ui.valuesTableView_Widget.setModel(self.table_model)  

        QtCore.QObject.connect(self.ui.valuesTableView_Widget, QtCore.SIGNAL("clicked(QModelIndex)"), self.colorSelect_Action)

        # Initialize the templates combo
        self.initTemplatesCombo()
        
        # Change the style of the color pushbuttons
        self.report.interface.setParameter('colorscale_start', '255:255:255') 
        self.report.interface.setParameter('colorscale_end', '255:0:0') 
        
        self.ui.color1Button_Widget.setStyleSheet("background-color:#FFFFFF; border: 1px solid #000000")
        self.ui.color2Button_Widget.setStyleSheet("background-color:#FF0000; border: 1px solid #000000")
        
        QtCore.QObject.connect(self.ui.color1Button_Widget, QtCore.SIGNAL('clicked()'), self.changeGradientStart_Action)
        QtCore.QObject.connect(self.ui.color2Button_Widget, QtCore.SIGNAL('clicked()'), self.changeGradientEnd_Action)
        
    def renderReport(self):
        ''' Validation function that calls the real renderReport function if everything is OK '''
        
        self.report.interface.setParameter('targetField_index', self.ui.featuredFieldcomboBox_Widget.currentIndex())
        self.report.interface.setParameter('targetField', self.ui.featuredFieldcomboBox_Widget.currentText())
        self.report.interface.setParameter('targetSector', self.ui.featuredSectorcomboBox_Widget.currentText())
        self.report.interface.setParameter('density', self.ui.densityCheckbox_Widget.isChecked())
        self.report.interface.setParameter('classes', self.ui.classesSpinBox_Widget.value())
        self.report.interface.setParameter('autoClassification', self.ui.autoClassificationCheckbox_Widget.isChecked())
        self.report.interface.setParameter('useLogscale', False) 
        self.report.interface.setParameter('render3dPath', self.ui.render3dpathLineEdit_Widget.text())
        self.report.interface.setParameter('render3dNameField', self.ui.render3dnameComboBox_Widget.currentText())
        self.report.interface.setParameter('render3dTitle', self.ui.render3dtitleLineEdit_Widget.text())

        ### We set the modes to be rendered ###
        modes = {}
        if self.ui.equalIntervalModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.EqualInterval] = "Equal Interval"
        if self.ui.quantileModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.Quantile] = "Quantile"
        if self.ui.naturalBreaksModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.Jenks] = "Natural Breaks / Jenks"
        if self.ui.stdDeviationModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.StdDev] = "Standard Deviation"
        if self.ui.prettyBreaksModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.Pretty] = "Pretty Breaks"
        if self.ui.customModeCheckBox_Widget.isChecked():
            modes[QgsGraduatedSymbolRendererV2.Custom] = "Custom"      

        if self.ui.render3dModeCheckBox_Widget.isChecked():
            self.report.interface.setParameter('render3d', True)  
        else:
            self.report.interface.setParameter('render3d', False)  
            
        if self.ui.render2dModeCheckBox_Widget.isChecked():
            self.report.interface.setParameter('render2d', True)  
        else:
            self.report.interface.setParameter('render2d', False)

        # Validations
        if not self.ui.render3dModeCheckBox_Widget.isChecked() and not self.ui.render2dModeCheckBox_Widget.isChecked():
            msgBox = QMessageBox.warning(self, "Error", "You have to select at least one of the 2D or 3D modes.")
        elif modes.keys() == []:
            msgBox = QMessageBox.warning(self, "Error", "At least one mode has to be selected.")
        elif self.ui.customModeCheckBox_Widget.isChecked() and self.table_model.rowCount() == 0:
            msgBox = QMessageBox.warning(self, "Error", "At least one value has to be created if you select Custom mode.")
        else:
            for mode in modes.keys():
                # Finally we render the report
                self.report.interface.setParameter('mode', mode)
                self.report.interface.setParameter('mode_title', modes[mode])
 
                #layer = self.report.copyLayer(self.report.map_layer)
                layer = self.report.map_layer
                
                ### We set the range list of the custom mode parameter ###
                rangeList = []
                # Model of the tableview with the range data
                model = self.table_model
                for row in range(model.rowCount()):
                    data = []
                    for column in range(model.columnCount()):
                        index = model.index(row, column)
                        # We suppose data are strings
                        data.append(str(model.data(index)))
                    
                    bottomValue = float(data[0])
                    topValue = float(data[1])
                    label = str(data[2])
                    color = self.report.strToQColor(str(data[3]))
                    
                    # lambda function for random color generation
                    rangeList.append( self.report.makeSymbologyForRange( layer.geometryType(), bottomValue, topValue, label, color ) )  
                    
                self.report.interface.setParameter('rangeList', rangeList)

                self.report.renderReport(layer)
                
            self.close() 
        
    def toggleValuesColumnView(self):
        ''' This method enable and disable widgets in case we need the custom value mode '''
        if self.ui.customModeCheckBox_Widget.isChecked():
            self.ui.customValuesLabel_Widget.setEnabled(True)
            self.ui.valuesTableView_Widget.setEnabled(True)
            self.ui.addValueButton_Widget.setEnabled(True)
            self.ui.removeValueButton_Widget.setEnabled(True)
            self.ui.newTemplateButton_Widget.setEnabled(True)
            self.ui.saveTemplateButton_Widget.setEnabled(True)
            self.ui.templatesComboBox_Widget.setEnabled(True)
            self.ui.deleteTemplateButton_Widget.setEnabled(True)
        else:
            self.ui.customValuesLabel_Widget.setEnabled(False)
            self.ui.valuesTableView_Widget.setEnabled(False)
            self.ui.addValueButton_Widget.setEnabled(False)
            self.ui.removeValueButton_Widget.setEnabled(False)
            self.ui.newTemplateButton_Widget.setEnabled(False)
            self.ui.saveTemplateButton_Widget.setEnabled(False)
            self.ui.templatesComboBox_Widget.setEnabled(False)
            self.ui.deleteTemplateButton_Widget.setEnabled(False)
            
    def toggleRender3dView(self):
        ''' This method enable and disable widgets in case we need the custom value mode '''
        if self.ui.render3dModeCheckBox_Widget.isChecked():
            self.ui.render3dLabel_Widget.setEnabled(True)
            self.ui.render3dpathLabel_Widget.setEnabled(True)
            self.ui.render3dpathLineEdit_Widget.setEnabled(True)
            self.ui.render3dpushButton_Widget.setEnabled(True)
            self.ui.render3dnameLabel_Widget.setEnabled(True)
            self.ui.render3dnameComboBox_Widget.setEnabled(True)
            self.ui.render3dtitleLineEdit_Widget.setEnabled(True)
            self.ui.render3dtitleLabel_Widget.setEnabled(True)
        else:
            self.ui.render3dLabel_Widget.setEnabled(False)
            self.ui.render3dpathLabel_Widget.setEnabled(False)
            self.ui.render3dpathLineEdit_Widget.setEnabled(False)
            self.ui.render3dpushButton_Widget.setEnabled(False)
            self.ui.render3dnameLabel_Widget.setEnabled(False)
            self.ui.render3dnameComboBox_Widget.setEnabled(False)
            self.ui.render3dtitleLineEdit_Widget.setEnabled(False)
            self.ui.render3dtitleLabel_Widget.setEnabled(False)
            
            
    def selectRender3dPath_Action(self):
        ''' Display browse file dialog '''
        fname = QtGui.QFileDialog.getExistingDirectory(self, 'Select directory')
        if fname:
            self.ui.render3dpathLineEdit_Widget.setText(fname)
    
    def colorSelect_Action(self, qmodelindex):
    
        col_index = qmodelindex.column()

        if col_index == self.table_model.columnCount() - 1:
           
            color = color = self.report.strToQColor(str(qmodelindex.data(QtCore.Qt.DisplayRole)))
            col = QtGui.QColorDialog.getColor(color , self)

            if col.isValid():
                self.table_model.setData(qmodelindex, str(col.red())+":"+str(col.green())+":"+str(col.blue()))
                std_item = self.table_model.item(qmodelindex.row(), qmodelindex.column())
                brush = QtGui.QBrush(QtGui.QColor(col.red(), col.green(), col.blue()))
                std_item.setBackground(brush)
    
    def changeGradientStart_Action(self):
    
        color_str = self.report.interface.getParameter('colorscale_start')
        color = self.report.strToQColor(color_str)
        col = QtGui.QColorDialog.getColor(color , self)

        if col.isValid():
            self.ui.color1Button_Widget.setStyleSheet("background-color:rgb("+str(col.red())+","+str(col.green())+","+str(col.blue())+"); border: 1px solid #000000")
            self.report.interface.setParameter('colorscale_start',str(col.red())+":"+str(col.green())+":"+str(col.blue()))
            
    def changeGradientEnd_Action(self):
    
        color_str = self.report.interface.getParameter('colorscale_end')
        color = self.report.strToQColor(color_str)
        col = QtGui.QColorDialog.getColor(color , self)

        if col.isValid():
            self.ui.color2Button_Widget.setStyleSheet("background-color:rgb("+str(col.red())+","+str(col.green())+","+str(col.blue())+"); border: 1px solid #000000")
            self.report.interface.setParameter('colorscale_end',str(col.red())+":"+str(col.green())+":"+str(col.blue()))
        
    def clearTableView(self):
        self.table_model.removeRows(0, self.table_model.rowCount())
        
    def addCustomValue(self):
        ''' Add a new custom value to the table view model '''
        r = lambda: random.randint(0,255)
        red = r()
        green = r()
        blue = r()
        color_item = QStandardItem(str(red)+":"+str(green)+":"+str(blue))
        brush = QtGui.QBrush(QtGui.QColor(red, green, blue))
        brush.setStyle(QtCore.Qt.SolidPattern)
        color_item.setBackground(brush)
        data = [QStandardItem("0"), QStandardItem("0"), QStandardItem("0 - 0"), color_item]
        
        self.table_model.appendRow(data)
        
    def loadTemplate(self):
        current_index = self.ui.templatesComboBox_Widget.currentText()
        if current_index in self.custom_values_templates:
            self.clearTableView()
            data = self.custom_values_templates[current_index]
            
            for row in data:
                item = []
                
                col_number = 0
                for col in row:
                    # If is the last column we fill with the color
                    std_item = None
                    if col_number == len(row)-1:
                        std_item = QStandardItem(col)
                        color = self.report.strToQColor(str(col))
                        brush = QtGui.QBrush(color)
                        brush.setStyle(QtCore.Qt.SolidPattern)
                        std_item.setBackground(brush)
                    else:
                        std_item = QStandardItem(col)
                    
                    item.append(std_item)
                    
                    col_number = col_number + 1
                
                self.table_model.appendRow(item)
        else:
            self.clearTableView()
            
    
    def saveTemplate(self):
        current_index = self.ui.templatesComboBox_Widget.currentText()
        if current_index in self.custom_values_templates:
            # Model of the tableview with the range data
            rows = []
            for row in range(self.table_model.rowCount()):
                cols = []
                for column in range(self.table_model.columnCount()):
                    index = self.table_model.index(row, column)
                    # We suppose data are strings
                    cols.append(str(self.table_model.data(index)))
                rows.append(cols)
            self.custom_values_templates[self.ui.templatesComboBox_Widget.currentText()] = rows
            self.saveCurrentTemplates()
            
    def newTemplate(self):
        # Display dialog box asking for the template name
        self.prompt_dlg = ThematicMapReportPrompt_Dialog(self)
        if self.prompt_dlg.exec_():
            name = self.prompt_dlg.name
            self.prompt_dlg.close()
            # Save the new template to settings
            self.custom_values_templates[name] = []
            self.saveCurrentTemplates()
            # Show it in the combo
            self.ui.templatesComboBox_Widget.addItem(name, name)
            self.ui.templatesComboBox_Widget.setCurrentIndex(self.ui.templatesComboBox_Widget.count()-1)
            self.clearTableView()
            
    def deleteTemplate(self):
        current_template_index = self.ui.templatesComboBox_Widget.currentIndex()
        current_template = self.ui.templatesComboBox_Widget.currentText()
        if current_template in self.custom_values_templates:
            self.custom_values_templates.pop(current_template)
            self.ui.templatesComboBox_Widget.removeItem(current_template_index)
            # Save new config
            self.saveCurrentTemplates()
        
    def removeCustomValue(self):
        indexes = self.ui.valuesTableView_Widget.selectedIndexes()
        for index in indexes:
            self.table_model.removeRow(index.row())
            
            
    def initTemplatesCombo(self):
        # We get the custom values templates saved in the INI file
        self.custom_values_templates = self.report.settings.value("thematic/custom_values_templates", {} )
        # Fill the combo with them
        self.ui.templatesComboBox_Widget.addItem("", "")
        for key in self.custom_values_templates.keys():
            self.ui.templatesComboBox_Widget.addItem(key, key)
            
    def saveCurrentTemplates(self):
        self.report.settings.setValue("thematic/custom_values_templates", self.custom_values_templates)      

      
class ThematicMapReportPrompt_Dialog(QtGui.QDialog):
    ''' Class for asking for the custom values template name '''
    def __init__(self, parent):
        QtGui.QDialog.__init__(self, parent)
        # Set up the user interface from Designer.
        self.ui = Ui_ThematicMapReportPromptDialog()
        self.ui.setupUi(self)
        self.parent = parent
        self.name = ""
        
        # Signal for saving the name
        QtCore.QObject.connect(self.ui.saveButton_Widget, QtCore.SIGNAL('clicked()'), self.loadName)
        QtCore.QObject.connect(self.ui.cancelButton_Widget, QtCore.SIGNAL('clicked()'), self.reject)
        
    def loadName(self):
        name = self.ui.nameLineEdit_Widget.text()
        if name == "":
            msgBox = QMessageBox.warning(self, "Error", "The name cannot be empty.")
        elif name in self.parent.custom_values_templates:
            msgBox = QMessageBox.warning(self, "Error", "That name is already in use.")
        else:
            self.name = self.ui.nameLineEdit_Widget.text()
            self.accept()
    
    def getName(self):
        return self.name