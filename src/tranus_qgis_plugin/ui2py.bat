@echo off
set PYUIC_PATH=C:\Python27\ArcGIS10.1\Lib\site-packages\PyQt4\pyuic4
set TARGET_PATH=C:\Users\pinzaghi\INRIA\qgistranus\src\tranus_qgis_plugin

"%PYUIC_PATH%" -o "%TARGET_PATH%\reports\thematic_map_report_ui.py" "%TARGET_PATH%\reports\thematic_map_report.ui" & "%PYUIC_PATH%" -o "%TARGET_PATH%\generate_report_ui.py" "%TARGET_PATH%\generate_report.ui" & "%PYUIC_PATH%" -o "%TARGET_PATH%\reports\thematic_map_report_prompt_ui.py" "%TARGET_PATH%\reports\thematic_map_report_prompt.ui" & "%PYUIC_PATH%" -o "%TARGET_PATH%\reports\one_to_many_report_ui.py" "%TARGET_PATH%\reports\one_to_many_report.ui" & "%PYUIC_PATH%" -o "%TARGET_PATH%\reports\one_to_many_report_prompt_ui.py" "%TARGET_PATH%\reports\one_to_many_report_prompt.ui"