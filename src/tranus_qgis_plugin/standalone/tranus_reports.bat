@echo off

set OSGEO4W_ROOT=C:\OSGeo4W64
set PLUGIN_PATH=C:\Users\TieuMy\Desktop\QGIS_TRANUS_Plugin

set QGISHOME=%OSGEO4W_ROOT%\apps\qgis
set PYTHONPATH=%PYTHONPATH%;%OSGEO4W_ROOT%\apps\qgis\python
set REPORTMODS_PATH=%PLUGIN_PATH%\src\tranus_qgis_plugin\reports
call "%OSGEO4W_ROOT%\bin\o4w_env.bat"
call "%OSGEO4W_ROOT%\apps\grass\grass-6.4.3\etc\env.bat
@echo off
path %OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\grass\grass-6.4.3\lib;%PATH%
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis
set GDAL_FILENAME_IS_UTF8=YES
rem Set VSI cache to be used as buffer, see #6448
set VSI_CACHE=TRUE
set VSI_CACHE_SIZE=1000000

cd 
start python "%PLUGIN_PATH%\standalone\tranus_reports.py" %*