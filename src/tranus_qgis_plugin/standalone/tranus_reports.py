import sys
import os
import argparse
import random

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *

sys.path.append(os.path.dirname(os.path.abspath(__file__))+'\..')

# Import our GUI
from report_display_ui import Ui_MainWindow

from reports.base_report import *
from reports.thematic_report import *
from reports.one_to_many_report import *
from interface import *

class ReportDisplay_Dialog(QMainWindow, Ui_MainWindow):

    def __init__(self):

        reload(reports.thematic_report)
        
        QMainWindow.__init__(self)

        # Required by Qt4 to initialize the UI
        self.setupUi(self)
        
        # Set the title for the app
        self.setWindowTitle("Report display")

        # Create the map canvas
        self.canvas = QgsMapCanvas()
        self.canvas.useImageToRender(False)
        
        # Lay our widgets out in the main window using a 
        # vertical box layout
        self.layout = QVBoxLayout(self.frame)
        self.layout.addWidget(self.canvas)

def main(argv):

    # Environment variable QGISHOME must be set to the install directory
    # before running the application
    qgis_prefix = os.getenv("QGISHOME")
    
    # Initialize qgis libraries
    QgsApplication.setPrefixPath(qgis_prefix, True)
    QgsApplication.initQgis()
  
    # Arguments parsing
    parser = argparse.ArgumentParser(description="Command line script for display the outcomes of TRANUS")
    
    parser.add_argument('-d', '--delimiter', help='Field delimiter in the report file, by default is a comma.', default=',')
    parser.add_argument('-a', '--axis', help='Zones axis in the report file, by default every zone is a new row. Valid options are "row" or "column"', default='row')
    parser.add_argument('-w', '--window', help='This flag enable the result to be showed in a window.', action='store_true')
    
    subparsers = parser.add_subparsers(dest="subparser_name")

    # create the parser for the "thematic" command
    parser_thematic = subparsers.add_parser('thematic', help='Graduated map report.')
    parser_thematic.add_argument('basemap', type=str, help='File path to the map layer used as base')
    parser_thematic.add_argument('report', type=str, help='File path to the report file')
    parser_thematic.add_argument('joinidx', type=int, help='Field index of the map layer to join with the report file')
    parser_thematic.add_argument('joinidx2', type=int, help='Index of the field in the report file to be joined with the field explicited in "joinidx"')
    parser_thematic.add_argument('mode', type=int, help='Number of graduated map mode, options are: Equal Interval=0, Quantile=1, Natural Breaks / Jenks=2, Standard Deviation=3, Pretty Breaks=4, Custom=5')
    parser_thematic.add_argument('sectoridx', type=int, help='Index of the sector in the report to create reports.')
    parser_thematic.add_argument('targetidx', type=int, help='Index of the field in the report to use as value.')
    parser_thematic.add_argument('classes', type=int, help='Number of classes to divide the range values.')
    parser_thematic.add_argument('--colorscale_start', type=str, help='Starting color for the gradient used in all modes except Custom e.g. --colorscale_start 0:255:0', default='0:255:0')
    parser_thematic.add_argument('--colorscale_end', type=str, help='Ending color for the gradient used in all modes except Custom e.g. --colorscale_end 255:0:0', default='255:0:0')
    parser_thematic.add_argument('--ranges', type=str, help='List of ranges values in the Custom mode. e.g. --range 1:3 4:15 16:100', nargs='+', default=[])
    parser_thematic.add_argument('--colors', type=str, help='List of colors values in the Custom mode. e.g. --colors 255:0:0 0:255:0 0:0:255', nargs='+', default=[])
    parser_thematic.add_argument('--template', type=str, help='Use saved list of custom values in the Custom mode. e.g. --template saved_template')
    parser_thematic.add_argument('--imagepath', type=str, help='Path to save the report image.', default='report.png')
    parser_thematic.add_argument('--render3d', help='This flag enable the result to be showed in a 3D render.', action='store_true')
    parser_thematic.add_argument('--render2d', help='This flag enable the result to be showed in a 2D render.', action='store_true')
    parser_thematic.add_argument('--render3d_path', type=str, help='Path to save the 3d report file.', default='')
    parser_thematic.add_argument('--render3d_namefield', type=str, help='Field of the map layer to be used to tag the zones in the report.', default='')
    parser_thematic.add_argument('--logscale', help='Display the results in a logarithm scale.', action='store_true')
    parser_thematic.add_argument('--render3d_title', type=str, help='Title for the 3D render.', default='')
    
    # create the parser for the "one to many" command
    parser_thematic = subparsers.add_parser('one2many', help='Comparison one to many between regions.')
    parser_thematic.add_argument('basemap', type=str, help='File path to the map layer used as base')
    parser_thematic.add_argument('report', type=str, help='File path to the report file')
    parser_thematic.add_argument('relations', type=str, help='File path to the file with the relations between zones.')
    parser_thematic.add_argument('joinidx', type=int, help='Field index of the map layer to join with the report file')
    parser_thematic.add_argument('joinidx2', type=int, help='Index of the field in the report file to be joined with the field explicited in "joinidx"')
    parser_thematic.add_argument('mode', type=int, help='Number of report mode, options are: Equal Interval=0, Custom=5')
    parser_thematic.add_argument('--colorscale_start', type=str, help='Starting color for the gradient used in all modes except Custom e.g. --colorscale_start 0:255:0', default='0:255:0')
    parser_thematic.add_argument('--colorscale_end', type=str, help='Ending color for the gradient used in all modes except Custom e.g. --colorscale_end 255:0:0', default='255:0:0')
    parser_thematic.add_argument('--ranges', type=str, help='List of ranges values in the Custom mode. e.g. --range 1:3 4:15 16:100', nargs='+', default=[])
    parser_thematic.add_argument('--colors', type=str, help='List of colors values in the Custom mode. e.g. --colors 255:0:0 0:255:0 0:0:255', nargs='+', default=[])
    parser_thematic.add_argument('--template', type=str, help='Use saved list of custom values in the Custom mode. e.g. --template saved_template')
    parser_thematic.add_argument('--imagepath', type=str, help='Path to save the report image.', default='report.png')
    parser_thematic.add_argument('--render3d_path', type=str, help='Path to save the 3d report file.', default='')
    parser_thematic.add_argument('--render3d_namefield', type=str, help='Field of the map layer to be used to tag the zones in the report.', default='')
    parser_thematic.add_argument('--render3d_title', type=str, help='Title for the 3D render.', default='')
    
    parsed_args = parser.parse_args()
        
    try:
        if parsed_args.subparser_name == 'thematic':
            thematicMapRoutine(argv, parsed_args)
        elif parsed_args.subparser_name == 'one2many':
            one2manyMapRoutine(argv, parsed_args)    
    except Exception as e:
        printError(str(e))
     
            
    sys.exit()
   
##############################
##### Auxiliar functions #####
##############################
   
def printError(error):
    print 'ERROR: '+error
    sys.exit()
    
def saveLayerToImage(layer, filename='report.png'):
    # create image
    img = QImage(QSize(800,600), QImage.Format_ARGB32_Premultiplied)

    # set image's background color
    color = QColor(255,255,255)
    img.fill(color.rgb())

    # create painter
    p = QPainter()
    p.begin(img)
    p.setRenderHint(QPainter.Antialiasing)

    render = QgsMapRenderer()

    # set layer set
    lst = [ layer.id() ]  # add ID of every layer
    render.setLayerSet(lst)

    # set extent
    rect = QgsRectangle(render.fullExtent())
    rect.scale(1.1)
    render.setExtent(rect)

    # set output size
    render.setOutputSize(QSize(800,600), img.logicalDpiX())
    
    # do the rendering
    render.render(p)
    p.end()
    
    # save image
    img.save(filename,"png")
   
    
def getRangeListFromArgs(parsed_args, geometryType, report):
    # Create the list of ranges for Custom mode
    rangeList = []
    if hasattr(parsed_args, 'ranges') and len(parsed_args.ranges)>0:
        # If the user set the colors we check that the length of the lists are the same
        if hasattr(parsed_args, 'colors') and len(parsed_args.colors)>0 and not len(parsed_args.colors)==len(parsed_args.ranges):
            raise ReportError('The quantity of colors and ranges does not match.')
            
        range_index = 0
        for range in parsed_args.ranges:
            # The color is defined by the user
            if hasattr(parsed_args, 'colors') and len(parsed_args.colors)>0:
                color = parsed_args.colors[range_index]
                values = color.split(':')
                if len(values) == 3:
                    r = int(values[0])
                    g = int(values[1])
                    b = int(values[2])
                # Bad format problem
                else:
                    raise ReportError('The color values format is "red:green:blue"')
            # The color is not defined, we create one randomly
            else:
                r = random.randint(0,255)
                g = random.randint(0,255)
                b = random.randint(0,255)
            
            values = range.split(':')
            if len(values) == 2:
                bottomValue = float(values[0])
                topValue = float(values[1])
                label = range
                rangeList.append( ThematicMapReport.makeSymbologyForRange( geometryType, bottomValue, topValue, label, QColor(r,g,b) ) )
            # Bad format problem
            else:
                raise ReportError('The range values format is "bottomVal:topVal"')
            
            range_index = range_index+1
    # Loading the range of values from qsettings
    elif hasattr(parsed_args, 'template') and not parsed_args.template == None:
        dict = report.settings.value("thematic/custom_values_templates", {} )
        templates =  QtCore.QVariant(dict).toMap()
        key = QtCore.QString(parsed_args.template)
        
        #if hasattr(templates, QtCore.QString(parsed_args.template)):
        if key in templates:
            for values_variant in templates[key].toList():
                values = values_variant.toList()
                color_str = values_variant.toList()[3].toString()
                color = report.strToQColor(str(color_str))
                rangeList.append( ThematicMapReport.makeSymbologyForRange( geometryType, int(values[0].toString()), int(values[1].toString()), values[2].toString(), color ) )
        else:
            raise ReportError('Invalid template: '+parsed_args.template)
            
    return rangeList

def thematicMapRoutine(argv, parsed_args):
    
    interface = Interface()
    interface.setParameter('join_field_idx', parsed_args.joinidx) 
    interface.setParameter('join2_field_idx', parsed_args.joinidx2) 
    interface.setParameter('mode', parsed_args.mode) 
    interface.setParameter('mode_title', ThematicMapReport.MODES_TITLES[parsed_args.mode])
    interface.setParameter('targetSector_index', parsed_args.sectoridx) 
    interface.setParameter('targetField_index', parsed_args.targetidx) 
    interface.setParameter('classes', parsed_args.classes) 
    interface.setParameter('useLogscale', parsed_args.logscale)
    interface.setParameter('render3d', parsed_args.render3d)
    interface.setParameter('render2d', parsed_args.render2d)
    interface.setParameter('render3dPath', parsed_args.render3d_path)
    interface.setParameter('render3dNameField', parsed_args.render3d_namefield)
    interface.setParameter('render3dTitle', parsed_args.render3d_title)
    interface.setParameter('colorscale_start', parsed_args.colorscale_start)
    interface.setParameter('colorscale_end', parsed_args.colorscale_end)

    try:
        map_handler = open(parsed_args.basemap, 'r')
    except Exception as e:
        raise ReportError('Failed to open Base map file: '+parsed_args.basemap)
        
    layer = QgsVectorLayer(path=parsed_args.basemap, baseName='Map', providerLib='ogr')
        
    report = BaseReport(interface=interface, report_file=parsed_args.report, map_layer=layer, parent_widget=None)
    report.load(report_delimiter=parsed_args.delimiter, zones_axis=parsed_args.axis)
    report.__class__ = ThematicMapReport
    report.__init__(map_layer=layer ,report_file=parsed_args.report, parent_widget=None )
    
    interface.setParameter('rangeList', getRangeListFromArgs(parsed_args, layer.geometryType(), report) )
    
    # Where the magic begins...
    layers = report.renderReport(layer)

    # Add layer to the registry
    QgsMapLayerRegistry.instance().addMapLayer(layers['layer2d']);
    
    # We show the result in a window
    if parsed_args.window:
        # create Qt application
        app = QApplication(argv)

        # create main window
        window = ReportDisplay_Dialog()
        # Set extent to the extent of our layer  
        window.canvas.setExtent(layers['layer2d'].extent())

        # Set up the map canvas layer set   
        window.canvas.setLayerSet([ QgsMapCanvasLayer(layers['layer2d'])])
        window.canvas.show()
        window.show()
        # run!
        retval = app.exec_()
    elif parsed_args.render2d:
        # Otherwise, we save it in an image file   
        saveLayerToImage(layers['layer2d'], parsed_args.imagepath)
                
    # exit
    QgsApplication.exitQgis()
       
       
def one2manyMapRoutine(argv, parsed_args):

    interface = Interface()
    interface.setParameter('join_field_idx', parsed_args.joinidx) 
    interface.setParameter('join2_field_idx', parsed_args.joinidx2) 
    interface.setParameter('mode', parsed_args.mode) 
    interface.setParameter('relations_path', parsed_args.relations)
    interface.setParameter('render3dPath', parsed_args.render3d_path)
    interface.setParameter('render3dNameField', parsed_args.render3d_namefield)
    interface.setParameter('render3dTitle', parsed_args.render3d_title)
    interface.setParameter('colorscale_start', parsed_args.colorscale_start)
    interface.setParameter('colorscale_end', parsed_args.colorscale_end)
    interface.setParameter('mode_title', OneToManyReport.MODES_TITLES[parsed_args.mode])

    try:
        map_handler = open(parsed_args.basemap, 'r')
    except Exception as e:
        raise ReportError('Failed to open Base map file: '+parsed_args.basemap)
        
    layer = QgsVectorLayer(path=parsed_args.basemap, baseName='Map', providerLib='ogr')
    
    try:
        relations_handler = open(parsed_args.relations, 'r')
    except Exception as e:
        raise ReportError('Failed to open Relations file: '+parsed_args.relations)
        
    report = BaseReport(interface=interface, report_file=parsed_args.report, map_layer=layer, parent_widget=None)
    report.load(report_delimiter=parsed_args.delimiter, zones_axis=parsed_args.axis)
    report.__class__ = OneToManyReport
    report.__init__(map_layer=layer ,report_file=parsed_args.report, relations_file=parsed_args.relations , parent_widget=None )
    
    interface.setParameter('rangeList', getRangeListFromArgs(parsed_args, layer.geometryType(), report) )
    
    # Where the magic begins...
    report.renderReport(layer)
        

if __name__ == "__main__":
    main(sys.argv[1:])

