@echo off
set PYUIC_PATH=C:\Python27\ArcGIS10.1\Lib\site-packages\PyQt4\pyuic4
set TARGET_PATH=C:\Users\pinzaghi\INRIA\qgistranus\src\tranus_qgis_standalone

"%PYUIC_PATH%" -o "%TARGET_PATH%\report_display_ui.py" "%TARGET_PATH%\report_display_ui.ui"