"""
/***************************************************************************
    TranusReports

    A tool for showing the outcome of TRANUS tools in a friendly manner.
                             -------------------
        begin                : 2014-07-22
        copyright            : (C) 2011-2014 by INRIA
        email                : patricio.inzaghi@inria.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""
import sys

def classFactory(iface):
    # load TranusReports class from file tranusreports.py
    from tranusreports import TranusReports
    return TranusReports(iface)
