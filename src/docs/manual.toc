\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Installation}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Windows}{3}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Plugin}{3}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Standalone}{3}{subsubsection.2.1.2}
\contentsline {subsection}{\numberline {2.2}Linux}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Usage}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Standalone}{4}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Thematic report}{4}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}One to many report}{6}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}QGIS plugin}{9}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Thematic report}{9}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}One to many report}{14}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Color templates}{19}{subsubsection.3.2.3}
\contentsline {section}{\numberline {4}Developement}{20}{section.4}
\contentsline {subsection}{\numberline {4.1}Developement}{20}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Structure}{20}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Creating new reports}{20}{subsubsection.4.1.2}
